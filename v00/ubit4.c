#include <stdio.h>

typedef union {
	unsigned raw :4;
	struct {
		unsigned ubit :1;
		signed number :3;
	} unum __attribute__ ((packed));
	/*
	struct {
		unsigned lower :3;
		unsigned sign :1;
	} overop __attribute__ ((packed));
	*/
} ubit4_t;

#define BINFMT "%c%c%c%c"
#define BIN(n) (n&0b1000?'1':'0'),(n&0b0100?'1':'0'),(n&0b0010?'1':'0'),(n&0b0001?'1':'0')
#define PRINT(desc, n) printf("%s [num=%s] h=0x%1x, b=0b"BINFMT", ubit=%c\n", desc, LOOKUP[n.raw], n.raw, BIN(n.raw), n.unum.ubit?'1':'0')

char LOOKUP[][16] = {
	[0b0000] = "0",
	[0b0001] = "(0,/2)",
	[0b0010] = "/2",
	[0b0011] = "(/2,1)",
	[0b0100] = "1",
	[0b0101] = "(1,2)",
	[0b0110] = "2",
	[0b0111] = "(2,/0)",

	[0b1111] = "(0,-/2)",
	[0b1110] = "-/2",
	[0b1101] = "(-/2,-1)",
	[0b1100] = "-1",
	[0b1011] = "(-1,-2)",
	[0b1010] = "-2",
	[0b1001] = "(-2,/0)",
	[0b1000] = "/0",
};

/* SORN ??
#define PLUSRAW(a,b) \
	(a==0b1000 && b==0b1000? 0b1111, 0)

#define PLUS(a,b) PLUSRAW(a.raw,b.raw)
*/

#define NEG(n) (~n.raw) + 1
#define OVER(n) ((~n.raw & 0b0111) | (n.raw & 0b1000)) + 1


int main(int argc, char const *argv[])
{
	ubit4_t one = {0b0100};
	PRINT("one", one);

	ubit4_t negone = {NEG(one)};
	PRINT("negative one", negone);


	ubit4_t two = {0b0110};
	PRINT("two", two);

	ubit4_t negtwo = {NEG(two)};
	PRINT("negative two", negtwo);

	ubit4_t overnegtwo = {OVER(negtwo)};
	PRINT("over negative two", overnegtwo);

	ubit4_t overtwo = {NEG(overnegtwo)};
	PRINT("over two", overtwo);


	ubit4_t zero = {0b0000};
	PRINT("zero", zero);

	ubit4_t overzero = {OVER(zero)};
	PRINT("over zero", overzero);

	return 0;
}
