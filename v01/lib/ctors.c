#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <stdio.h>

#include <unum.h>
#include <unum-priv.h>

#define max(a,b) ({ typeof(a) __a = (a); typeof(b) __b = (b); (__a > __b)? __a : __b; })

unumd_t * unum_new(unsigned int ndec, unsigned int nbits)
{
	__label__ memfail;

	unumd_t * ud = malloc(sizeof(unumd_t));
	if (ud == NULL)
	{
		// Malloc failed
		return NULL;
	}

	// TODO - properly compute accuracy (very hacky at the moment)
	*ud = (unumd_t){ .nbits = nbits, .ns = pow(2,nbits), .mask = (0x1 << nbits) - 1, .accuracy = pow(10, ndec+1) };

#if USE_EXACTS == _FRACTIONS
	// Populate exacts fraction list
	{
		size_t ne = ud->ns/2, i = 0;								// Size of exact array
		size_t ur = (pow(10,ndec) - pow(10,ndec-1)) * 2;			// Size of upper right quadrant (as specified by ndec)
		fraction_t * exactsf = malloc(max(ur, ne) * sizeof(fraction_t));
		if (exactsf == NULL)
		{
			goto memfail;
		}

		void compact()
		{
			// Sort list
			int compare_fractions(const void *a, const void *b)
			{
				const fraction_t *fa = a;
				const fraction_t *fb = b;
				return frac_gt(fa, fb) - frac_lt(fa, fb);
			}
			qsort(exactsf, i, sizeof(fraction_t), compare_fractions);

			// Remove duplicates
			for (size_t j = 0; j < i-1; j++)
			{
				if (frac_eq(&exactsf[j], &exactsf[j+1]))
				{
					size_t k = j + 2;
					while (frac_eq(&exactsf[k], &exactsf[j])) k++;
					memmove(&exactsf[j+1], &exactsf[k], (i-k) * sizeof(fraction_t));
					i -= k-j-1;
				}
			}
		}

		// Create upper right quadrant
		{
			// M: Range[10^(ndec - 1), 10^ndec - 1]/10^(ndec - 1)
			for (unsigned long n = pow(10,ndec-1); n <= pow(10,ndec) - 1; n++)
			{
				exactsf[i++] = frac_new(n, pow(10,ndec-1));
			}

			// M: 10/exacts
			for (size_t j = 1, max = i; j < max; j++)
			{
				exactsf[i++] = frac_div(frac_new(10, 1), exactsf[j]);
			}

			compact();
		}

		// Complete upper right quadrant
		{
			exactsf = realloc(exactsf, ne * sizeof(fraction_t));
			if (exactsf == NULL)
			{
				goto memfail;
			}

			size_t max = pow(2, nbits - 3);
			while (i < max)
			{
				for (size_t j = 0; j < max; j++)
				{
					exactsf[i++] = frac_mul(frac_new(10, 1), exactsf[j]);
				}
				compact();
			}
			i = max;
		}

		// Generate recirpicals and negatives
		{
			for (size_t j = 0, max = i; j < max; j++)
			{
				exactsf[i++] = frac_recip(exactsf[j]);
			}

			// Generate negatives
			for (size_t j = 0, max = i; j < max; j++)
			{
				exactsf[i++] = frac_neg(exactsf[j]);
			}

			compact();
		}

		// Add zero and infinity
		{
			exactsf[i++] = frac_zero();
			exactsf[i++] = frac_inf();
			compact();
		}

		ud->exactsf = exactsf;
		ud->exactsf_zero = &exactsf[(ne-1)/2];
		ud->exactsf_max = &exactsf[ne-2];
	}

#elif USE_EXACTS == _DOUBLES
	// Populate exacts double list
	{
		size_t ne = ud->ns/2, i = 0;								// Size of exact array
		size_t ur = (pow(10,ndec) - pow(10,ndec-1)) * 2;			// Size of upper right quadrant (as specified by ndec)
		double * exactsd = malloc(max(ur, ne) * sizeof(double));
		if (exactsd == NULL)
		{
			goto memfail;
		}

		void compact()
		{
			// Sort list
			int compare_doubles(const void *a, const void *b)
			{
				const double *da = a;
				const double *db = b;
				return (*da > *db) - (*da < *db);
			}
			qsort(exactsd, i, sizeof(double), compare_doubles);

			// Remove duplicates
			for (size_t j = 0; j < i-1; j++)
			{
				if (exactsd[j] == exactsd[j+1])
				{
					size_t k = j + 2;
					while (exactsd[k] == exactsd[j]) k++;
					memmove(&exactsd[j+1], &exactsd[k], (i-k) * sizeof(double));
					i -= k-j-1;
				}
			}
		}

		// Create upper right quadrant
		{
			// M: Range[10^(ndec - 1), 10^ndec - 1]/10^(ndec - 1)
			for (unsigned long n = pow(10,ndec-1); n <= pow(10,ndec) - 1; n++)
			{
				exactsd[i++] = (double)n / pow(10,ndec-1);
			}

			// M: 10/exacts
			for (size_t j = 1, max = i; j < max; j++)
			{
				exactsd[i++] = 10.0 / exactsd[j];
			}

			compact();
		}

		// Complete upper right quadrant
		{
			exactsd = realloc(exactsd, ne * sizeof(double));
			if (exactsd == NULL)
			{
				goto memfail;
			}

			size_t max = pow(2,nbits - 3);
			while (i < max)
			{
				for (size_t j = 0; j < max; j++)
				{
					exactsd[i++] = 10.0 * exactsd[j];
				}
				compact();
			}
			i = max;
		}

		// Generate recirpicals and negatives
		{
			for (size_t j = 0, max = i; j < max; j++)
			{
				exactsd[i++] = 1.0 / exactsd[j];
			}

			// Generate negatives
			for (size_t j = 0, max = i; j < max; j++)
			{
				exactsd[i++] = -exactsd[j];
			}

			compact();
		}

		// Add zero and infinity
		{
			exactsd[i++] = 0;
			exactsd[i++] = INFINITY;
			compact();
		}

		ud->exactsd = exactsd;
		ud->exactsd_zero = &exactsd[(ne-1)/2];
		ud->exactsd_max = &exactsd[ne-2];
	}

#endif

#ifdef USE_ADDTBL
	// Populate addition table
	{
		size_t ns = ud->ns;
		sorn_t * addtable = ud->addtable = malloc(ns * ns * sizeof(sorn_t));
		if (addtable == NULL)
		{
			goto memfail;
		}

		for (unum_t a = 0; a < ns; a++)
		{
			for (unum_t b = 0; b < ns; b++)
			{
				#if 0
				{
					range_t ar = unum2range(ud, a);
					range_t br = unum2range(ud, b);
					range_t abr = sorn2range(ud, sorn_doadd(ud, unum2sorn(ud, a), unum2sorn(ud, b)));

					char ar_b[30], br_b[30], abr_b[30];
					range_snprintf(&ar, ar_b, sizeof(ar_b));
					range_snprintf(&br, br_b, sizeof(br_b));
					range_snprintf(&abr, abr_b, sizeof(abr_b));

					printf("%s + %s = %s\n", ar_b, br_b, abr_b);
				}
				#endif

				//fprintf(stderr, "a: 0x%x, b: 0x%x\n", a, b);
				//fflush(stderr);

				addtable[_lookup_tidx(a,b)] = sorn_doadd(ud, unum2sorn(ud, a), unum2sorn(ud, b));
			}
		}

	} 
#endif

#ifdef USE_MULTBL
	// Populate multiplication table
	{
		size_t ns = ud->ns;
		sorn_t * multable = ud->multable = malloc(ns * ns * sizeof(sorn_t));
		if (multable == NULL)
		{
			goto memfail;
		}

		for (unum_t a = 0; a < ns; a++)
		{
			for (unum_t b = 0; b < ns; b++)
			{
				#if 0
				{
					range_t ar = unum2range(ud, a);
					range_t br = unum2range(ud, b);
					range_t abr = sorn2range(ud, sorn_domul(ud, unum2sorn(ud, a), unum2sorn(ud, b)));

					char ar_b[30], br_b[30], abr_b[30];
					range_snprintf(&ar, ar_b, sizeof(ar_b));
					range_snprintf(&br, br_b, sizeof(br_b));
					range_snprintf(&abr, abr_b, sizeof(abr_b));

					printf("%s * %s = %s\n", ar_b, br_b, abr_b);
				}
				#endif

				//fprintf(stderr, "a: 0x%x, b: 0x%x\n", a, b);
				//fflush(stderr);

				multable[_lookup_tidx(a,b)] = sorn_domul(ud, unum2sorn(ud, a), unum2sorn(ud, b));
			}
		}
	}
#endif

	return ud;

memfail:
	unum_free(ud);
	return NULL;
}

unumd_t * unum_copy(const unumd_t * oud)
{
	__label__ memfail;

	unumd_t * ud = malloc(sizeof(unumd_t));
	if (ud == NULL)
	{
		return NULL;
	}

	*ud = (unumd_t){ .nbits = oud->nbits, .ns = oud->ns, .mask = (0x1 << oud->nbits) - 1, .accuracy = oud->accuracy };

#if USE_EXACTS == _FRACTIONS
	// Copy exact fractions
	{
		size_t ne = ud->ns/2;
		ud->exactsf = malloc(ne * sizeof(double));
		if (ud->exactsf == NULL)
		{
			goto memfail;
		}

		memcpy(ud->exactsf, oud->exactsf, ne * sizeof(double));
		ud->exactsf_zero = &ud->exactsf[(ne-1)/2];
		ud->exactsf_max = &ud->exactsf[ne-2];
	}

#elif USE_EXACTS == _DOUBLES
	// Copy exact doubles
	{
		size_t ne = ud->ns/2;
		ud->exactsd = malloc(ne * sizeof(double));
		if (ud->exactsd == NULL)
		{
			goto memfail;
		}

		memcpy(ud->exactsd, oud->exactsd, ne * sizeof(double));
		ud->exactsd_zero = &ud->exactsd[(ne-1)/2];
		ud->exactsd_max = &ud->exactsd[ne-2];
	}
#endif

#ifdef USE_ADDTBL
	// Copy addition table
	{
		size_t ns = ud->ns;
		ud->addtable = malloc(ns * ns * sizeof(sorn_t));
		if (ud->addtable == NULL)
		{
			goto memfail;
		}

		memcpy(ud->addtable, oud->addtable, ns * ns * sizeof(sorn_t));
	}
#endif

#ifdef USE_MULTBL
	// Copy multiplication table
	{
		size_t ns = ud->ns;
		ud->multable = malloc(ns * ns * sizeof(sorn_t));
		if (ud->multable == NULL)
		{
			goto memfail;
		}

		memcpy(ud->multable, oud->multable, ns * ns * sizeof(sorn_t));
	}
#endif

	return ud;

memfail:
	unum_free(ud);
	return NULL;
}

void unum_free(unumd_t * ud)
{
	if (ud == NULL) return;

#if USE_EXACTS == _FRACTIONS
	if (ud->exactsf != NULL)
	{
		free(ud->exactsf);
	}

#elif USE_EXACTS == _DOUBLES
	if (ud->exactsd != NULL)
	{
		free(ud->exactsd);
	}

#endif

#ifdef USE_ADDTBL
	if (ud->addtable != NULL)
	{
		free(ud->addtable);
	}
#endif

#ifdef USE_MULTBL
	if (ud->multable != NULL)
	{
		free(ud->multable);
	}
#endif

	memset(ud, 0, sizeof(unumd_t));
	free(ud);
}
