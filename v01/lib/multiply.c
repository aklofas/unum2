#include <unum.h>
#include <unum-priv.h>


#if USE_EXACTS == _FRACTIONS

#define _min2r(a,b,ar,br) (frac_lt(&a, &b)? ar : br)
#define min4(a) ({size_t _ia = _min2r(a[0],a[1],0,1), _ib = _min2r(a[2],a[3],2,3); _min2r(a[_ia],a[_ib],_ia,_ib); })

#define _max2r(a,b,ar,br) (frac_gt(&a, &b)? ar : br)
#define max4(a) ({ size_t _ia = _max2r(a[0],a[1],0,1), _ib = _max2r(a[2],a[3],2,3); _max2r(a[_ia],a[_ib],_ia,_ib); })

	// Sorn multiply using fractions
	sorn_t sorn_domul(const unumd_t * ud, sorn_t left, sorn_t right)
	{
		// Check for empty
		if (sorn_isempty(ud, &left) || sorn_isempty(ud, &right))
			return sorn_empty(ud);

		// Check for everything
		if (sorn_iseverything(ud, &left) || sorn_iseverything(ud, &right))
			return sorn_everything(ud);

		// Check for improper
		if (sorn_isimporper(ud, &left) || sorn_isimporper(ud, &right))
			return sorn_everything(ud);


		// Exact flags for inputs
		bool ei[] = {unum_isexact(ud, ulow(left)), unum_isexact(ud, uhigh(left)), unum_isexact(ud, ulow(right)), unum_isexact(ud, uhigh(right))};

		// Exact flags for possible outputs
		bool eo[] = {ei[0]&&ei[2], ei[0]&&ei[3], ei[1]&&ei[2], ei[1]&&ei[3]};

		// Unum inputs adjusted to be exacts
		unum_t eu[] = {_unum_cast(ud, ulow(left) - (ei[0]? 0 : 1)), _unum_cast(ud, uhigh(left) + (ei[1]? 0 : 1)), _unum_cast(ud, ulow(right) - (ei[2]?  0 : 1)), _unum_cast(ud, uhigh(right) + (ei[3]? 0 : 1))};

		// Fraction inputs
		fraction_t ef[] = {unum2exactfrac(ud, eu[0]), unum2exactfrac(ud, eu[1]), unum2exactfrac(ud, eu[2]), unum2exactfrac(ud, eu[3])};

		// Possible fraction outputs
		fraction_t out[] = {frac_mul(ef[0], ef[2]), frac_mul(ef[0], ef[3]), frac_mul(ef[1], ef[2]), frac_mul(ef[1], ef[3])};

		// Pick the bounds
		size_t lowidx = min4(out), highidx = max4(out);
		unum_t low = frac2unum(ud, out[lowidx]), high = frac2unum(ud, out[highidx]);

		// Re-adjust the exactness
		low += (unum_isexact(ud, low) && !eo[lowidx])? 1 : 0;
		high -= (unum_isexact(ud, high) && !eo[highidx])? 1 : 0;

		return sorn_new(ud, _unum_cast(ud, low), _unum_cast(ud, high), SORN_NOFLAGS);
	}

#elif USE_EXACTS == _DOUBLES

#define _min2r(a,b,ar,br) ((a < b)? ar : br)
#define min4(a) ({size_t _ia = _min2r(a[0],a[1],0,1), _ib = _min2r(a[2],a[3],2,3); _min2r(a[_ia],a[_ib],_ia,_ib); })

#define _max2r(a,b,ar,br) ((a > b)? ar : br)
#define max4(a) ({ size_t _ia = _max2r(a[0],a[1],0,1), _ib = _max2r(a[2],a[3],2,3); _max2r(a[_ia],a[_ib],_ia,_ib); })

	// Sorn multiply using doubles
	sorn_t sorn_domul(const unumd_t * ud, sorn_t left, sorn_t right)
	{
		// Check for empty
		if (sorn_isempty(ud, &left) || sorn_isempty(ud, &right))
			return sorn_empty(ud);

		// Check for everything
		if (sorn_iseverything(ud, &left) || sorn_iseverything(ud, &right))
			return sorn_everything(ud);

		// Check for improper
		if (sorn_isimporper(ud, &left) || sorn_isimporper(ud, &right))
			return sorn_everything(ud);

		// Exact flags for inputs
		bool ei[] = {unum_isexact(ud, ulow(left)), unum_isexact(ud, uhigh(left)), unum_isexact(ud, ulow(right)), unum_isexact(ud, uhigh(right))};

		// Exact flags for possible outputs
		bool eo[] = {ei[0]&&ei[2], ei[0]&&ei[3], ei[1]&&ei[2], ei[1]&&ei[3]};

		// Unum inputs adjusted to be exacts
		unum_t eu[] = {_unum_cast(ud, ulow(left) - (ei[0]? 0 : 1)), _unum_cast(ud, uhigh(left) + (ei[1]? 0 : 1)), _unum_cast(ud, ulow(right) - (ei[2]?  0 : 1)), _unum_cast(ud, uhigh(right) + (ei[3]? 0 : 1))};

		// Fraction inputs
		double ed[] = {unum2exactdouble(ud, eu[0]), unum2exactdouble(ud, eu[1]), unum2exactdouble(ud, eu[2]), unum2exactdouble(ud, eu[3])};

		// Possible fraction outputs
		double out[] = {ed[0] * ed[2], ed[0] * ed[3], ed[1] * ed[2], ed[1] * ed[3]};

		// Pick the bounds
		size_t lowidx = min4(out), highidx = max4(out);
		unum_t low = double2unum(ud, out[lowidx]), high = double2unum(ud, out[highidx]);

		// Re-adjust the exactness
		low += (unum_isexact(ud, low) && !eo[lowidx])? 1 : 0;
		high -= (unum_isexact(ud, high) && !eo[highidx])? 1 : 0;

		return sorn_new(ud, _unum_cast(ud, low), _unum_cast(ud, high), SORN_NOFLAGS);
	}

#endif

#if defined(USE_MULTBL)
	sorn_t sorn_mul(const unumd_t * ud, sorn_t left, sorn_t right)
	{
		size_t ns = ud->ns;
		return (sorn_t)
		{
			.low = ud->multable[_lookup_tidx(left.low, right.low)].low,
			.high = ud->multable[_lookup_tidx(left.high, right.high)].high
		};
	}

#else
	sorn_t sorn_mul(const unumd_t * ud, sorn_t left, sorn_t right)
	{
		return sorn_domul(ud, left, right);
	}
#endif
