#include <math.h>

#include <stdio.h>
//#include <errno.h>

#include <unum.h>
#include <unum-priv.h>

#define min(a,b) ({ typeof(a) __a = (a); typeof(b) __b = (b); (__a < __b)? __a : __b; })
#define sign(x) (((x) > 0) - ((x) < 0))
#define cast(u) ((unum_t)(u) & ud->mask)

// TODO - replace cast(u) with _unum_cast


#if USE_EXACTS == _FRACTIONS
	fraction_t unum2exactfrac(const unumd_t * ud, unum_t n)
	{
		unum_t mag = unum_abs(ud, n);
		return frac_mul(frac_new(unum_sgn(ud, n), 1), ud->exactsf_zero[mag>>1]);
	}

	unum_t frac2unum(const unumd_t * ud, fraction_t frac)
	{
		/*
		{
			char buf[50];
			frac_snprintf(&frac, buf, sizeof(buf));
			printf(">> %s\n", buf);
		}
		 */

		// TODO - high priority optimization target (pinpointed from callgraph)

		fraction_t mag = frac_abs(frac);

		fraction_t *exactsf_zero = ud->exactsf_zero, *exactsf_max = ud->exactsf_max;
		size_t ne = ud->ns/2;

		if (frac_gt(&mag, exactsf_max))
		{
			if (frac_isinf(&mag))
				return cast(ne);
			return cast(frac_sgn(&frac) * (ne-1));
		}

		size_t i = 0;
		while (!frac_isinf(&exactsf_zero[i+1]) && frac_geq(&mag, &exactsf_zero[i+1])) i++;
		unsigned ubit = (frac_neq(&mag, &exactsf_zero[i]))? 0x1 : 0x0;

		return cast(frac_sgn(&frac) * (i << 1 | ubit));
	}

	double unum2exactdouble(const unumd_t * ud, unum_t n)
	{
		return unum_sgn(ud, n) * frac2double(ud->exactsf_zero[unum_abs(ud, n) >> 1]);
	}

	unum_t double2unum(const unumd_t * ud, double x)
	{
		// Check for infinity
		if (fabs(x) == INFINITY)
		{
			return frac2unum(ud, frac_mul(frac_new(sign(x), 1), frac_inf()));
		}

		return frac2unum(ud, double2frac(x, ud->accuracy));
	}

#elif USE_EXACTS == _DOUBLES
	unum_t frac2unum(const unumd_t * ud, fraction_t frac)
	{
		return double2unum(ud, frac2double(frac));
	}

	double unum2exactdouble(const unumd_t * ud, unum_t n)
	{
		unum_t mag = unum_abs(ud, n);
		return unum_sgn(ud, n) * ud->exactsd_zero[mag>>1];
	}

	unum_t double2unum(const unumd_t * ud, double x)
	{
		// TODO - check for valud ud

		double mag = fabs(x);
		long long int magr = llrint(mag * (double)ud->accuracy);
		
		double *exactsd_zero = ud->exactsd_zero, *exactsd_max = ud->exactsd_max;
		size_t ne = ud->ns/2;

		if (mag == INFINITY || magr > llrint(*exactsd_max * (double)ud->accuracy))
		{
			if (mag == INFINITY)
				return cast(ne);
			return cast(sign(x) * (ne-1));
		}

		size_t i = 0;
		while (exactsd_zero[i+1] < INFINITY && magr >= llrint(exactsd_zero[i+1] * (double)ud->accuracy)) i++;
		unsigned ubit = (magr != llrint(exactsd_zero[i] * (double)ud->accuracy))? 0x1 : 0x0;

		return cast(sign(x) * (i << 1 | ubit));
	}

#endif

interval_t unum2interval(const unumd_t * ud, unum_t n)
{
	// TODO - handle everything flag

	if (unum_isexact(ud, n))
	{
		double x = unum2exactdouble(ud, n);
		return interval_new(closed, x, x, closed);
	}
	else
	{
		double low = unum2exactdouble(ud, cast(n-1)), high = unum2exactdouble(ud, cast(n+1));

		// Handle infinity
		{
			if (low == INFINITY)
				low *= unum_sgn(ud, n);

			if (high == INFINITY)
				high *= unum_sgn(ud, n);
		}

		return interval_new(open, low, high, open);
	}
}

interval_t sorn2interval(const unumd_t * ud, sorn_t s)
{
	// TODO - handle everything flag

	interval_t low = unum2interval(ud, ulow(s)), high = unum2interval(ud, uhigh(s));
	return interval_new(low.begin_cap, low.begin, high.end, high.end_cap);
}

