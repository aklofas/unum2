#include <unum.h>
#include <unum-priv.h>

#include <stdio.h>

#if USE_EXACTS == _FRACTIONS
	// Sorn add using fractions
	sorn_t sorn_doadd(const unumd_t * ud, sorn_t left, sorn_t right)
	{
		// Check for empty
		if (sorn_isempty(ud, &left) || sorn_isempty(ud, &right))
			return sorn_empty(ud);

		// Check for everything
		if (sorn_iseverything(ud, &left) || sorn_iseverything(ud, &right))
			return sorn_everything(ud);

		unum_t low, high;

		// Compute low unum
		{
			unum_t uleft = _unum_cast(ud, ulow(left) - (unum_isexact(ud, ulow(left))? 0 : 1));
			unum_t uright = _unum_cast(ud, ulow(right) - (unum_isexact(ud, ulow(right))?  0 : 1));
			fraction_t flow = frac_add(unum2exactfrac(ud, uleft), unum2exactfrac(ud, uright));

			low = frac2unum(ud, flow);
			low += unum_isexact(ud, low) && (!unum_isexact(ud, ulow(left)) || !unum_isexact(ud, ulow(right)))? 1 : 0;
		}

		// Compute high unum
		{
			unum_t uleft = _unum_cast(ud, uhigh(left) + (unum_isexact(ud, uhigh(left))? 0 : 1));
			unum_t uright = _unum_cast(ud, uhigh(right) + (unum_isexact(ud, uhigh(right))? 0 : 1));
			fraction_t fhigh = frac_add(unum2exactfrac(ud, uleft), unum2exactfrac(ud, uright));

			high = frac2unum(ud, fhigh);
			high -= unum_isexact(ud, high) && (!unum_isexact(ud, uhigh(left)) || !unum_isexact(ud, uhigh(right)))? 1 : 0;
		}

		return sorn_new(ud, _unum_cast(ud, low), _unum_cast(ud, high), SORN_NOFLAGS);

#if 0
		unum_t left_low = ulow(left), right_low = ulow(right), left_high = uhigh(left), right_high = uhigh(right);
		bool low_exact = unum_ispminf(ud, left_low) || unum_ispminf(ud, right_low) || (unum_isexact(ud, left_low) && unum_isexact(ud, right_low));
		bool high_exact = unum_ispminf(ud, left_high) || unum_ispminf(ud, right_high) || (unum_isexact(ud, left_high) && unum_isexact(ud, right_high));

		// Adjust the range to get exact unums
		left_low += unum_isexact(ud, left_low)? 0 : -1;
		right_low += unum_isexact(ud, right_low)? 0 : -1;
		left_high += unum_isexact(ud, left_high)? 0 : 1;
		right_high += unum_isexact(ud, right_high)? 0 : 1;

		fraction_t low = frac_add(unum2exactfrac(ud, left_low), unum2exactfrac(ud, right_low));
		fraction_t high = frac_add(unum2exactfrac(ud, left_high), unum2exactfrac(ud, right_high));

		sorn_t result = fracs2sorn(ud, low, high);

		// Adjust the range to re-instate inexact unums
		ulow(result) += unum_isexact(ud, ulow(result)) && !low_exact? 1 : 0;
		uhigh(result) += unum_isexact(ud, uhigh(result)) && !high_exact? -1 : 0;

		return result;
#endif
	}

#elif USE_EXACTS == _DOUBLES
	// Sorn add using doubles
	sorn_t sorn_doadd(const unumd_t * ud, sorn_t left, sorn_t right)
	{
		// Check for empty
		if (sorn_isempty(ud, &left) || sorn_isempty(ud, &right))
			return sorn_empty(ud);

		// Check for everything
		if (sorn_iseverything(ud, &left) || sorn_iseverything(ud, &right))
			return sorn_everything(ud);

		unum_t low, high;

		// Compute low unum
		{
			unum_t uleft = _unum_cast(ud, ulow(left) - (unum_isexact(ud, ulow(left))? 0 : 1));
			unum_t uright = _unum_cast(ud, ulow(right) - (unum_isexact(ud, ulow(right))?  0 : 1));
			double dlow = unum2exactdouble(ud, uleft) + unum2exactdouble(ud, uright);

			low = double2unum(ud, dlow);
			low += unum_isexact(ud, low) && (!unum_isexact(ud, ulow(left)) || !unum_isexact(ud, ulow(right)))? 1 : 0;
		}

		// Compute high unum
		{
			unum_t uleft = _unum_cast(ud, uhigh(left) + (unum_isexact(ud, uhigh(left))? 0 : 1));
			unum_t uright = _unum_cast(ud, uhigh(right) + (unum_isexact(ud, uhigh(right))? 0 : 1));
			double dhigh = unum2exactdouble(ud, uleft) + unum2exactdouble(ud, uright);

			high = double2unum(ud, dhigh);
			high -= unum_isexact(ud, high) && (!unum_isexact(ud, uhigh(left)) || !unum_isexact(ud, uhigh(right)))? 1 : 0;
		}

		return sorn_new(ud, _unum_cast(ud, low), _unum_cast(ud, high), SORN_NOFLAGS);

#if 0
		unum_t left_low = ulow(left), right_low = ulow(right), left_high = uhigh(left), right_high = uhigh(right);
		bool low_exact = unum_isexact(ud, left_low) && unum_isexact(ud, right_low), high_exact = unum_isexact(ud, left_high) && unum_isexact(ud, right_high);

		// Adjust the range to get exact unums
		left_low += unum_isexact(ud, left_low)? 0 : -1;
		right_low += unum_isexact(ud, right_low)? 0 : -1;
		left_high += unum_isexact(ud, left_high)? 0 : 1;
		right_high += unum_isexact(ud, right_high)? 0 : 1;

		double low = unum2exactdouble(ud, left_low) + unum2exactdouble(ud, right_low);
		double high = unum2exactdouble(ud, left_high) + unum2exactdouble(ud, right_high);

		sorn_t result = doubles2sorn(ud, low, high);

		// Adjust the range to re-instate inexact unums
		ulow(result) += unum_isexact(ud, ulow(result)) && !low_exact? 1 : 0;
		uhigh(result) += unum_isexact(ud, uhigh(result)) && !high_exact? -1 : 0;

		return result;
#endif
	}

#endif

#if defined(USE_ADDTBL)
	sorn_t sorn_add(const unumd_t * ud, sorn_t left, sorn_t right)
	{
		size_t ns = ud->ns;
		return (sorn_t)
		{
			.low = ulow(ud->addtable[_lookup_tidx(ulow(left), ulow(right))]),
			.high = uhigh(ud->addtable[_lookup_tidx(uhigh(left), uhigh(right))])
		};
	}

#else
	sorn_t sorn_add(const unumd_t * ud, sorn_t left, sorn_t right)
	{
		return sorn_doadd(ud, left, right);
	}
#endif
