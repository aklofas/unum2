#include <stdio.h>
#include <math.h>

#include <interval.h>

#define min(a,b) ({ typeof(a) __a = (a); typeof(b) __b = (b); (__a < __b)? __a : __b; })
#define max(a,b) ({ typeof(a) __a = (a); typeof(b) __b = (b); (__a > __b)? __a : __b; })
#define swap(a,b) ({ typeof(a) __c = (a); a = b; b = __c; })

/*
bool inrange(const interval_t * ival, double x, unsigned long accuracy)
{
#if 1
	long long int xr = llrint(x * (double)accuracy);

	// Handle low
	{
		if (fabs(ival->begin) == INFINITY)
		{
			if (ival->begin_cap == open && ival->begin == -INFINITY && x == -INFINITY)
			{
				return false;
			}

			if (ival->begin == INFINITY && x != INFINITY)
			{
				return false;
			}
		}
		else
		{
			long long int begin = llrint(ival->begin * (double)accuracy);
			if ((ival->begin_cap == open && xr <= begin) || (ival->begin_cap == closed && xr < begin))
			{
				//printf("Failed lower %.50f < %.50f (%lld < %lld)\n", x, range->begin, xr, begin);
				return false;
			}
		}
	}

	// Handle high
	{
		if (fabs(ival->end) == INFINITY)
		{
			if (ival->end_cap == open && ival->end == INFINITY && x == INFINITY)
			{
				return false;
			}

			if (ival->end == -INFINITY && x != -INFINITY)
			{
				return false;
			}
		}
		else
		{
			long long int end = llrint(ival->end * (double)accuracy);
			if ((ival->end_cap == open && xr >= end) || (ival->end_cap == closed && xr > end))
			{
				//printf("Failed upper %.50f > %.50f (%lld > %lld)\n", x, range->end, xr, end);
				return false;
			}
		}
	}

	return true;
#endif

#if 0
	double xr = rint(x * (double)accuracy);

	// Hangle low
	{
		if (fabs(ival->low) == INFINITY)
		{
			if (ival->begin_cap == open && ival->low == -INFINITY && x == -INFINITY)
			{
				return false;
			}

			if (ival->low == INFINITY && x != INFINITY)
			{
				return false;
			}
		}
		else
		{
			if ((ival->begin_cap == open && xr <= (ival->low * (double)accuracy)) || (ival->begin_cap == closed && xr < (ival->low * (double)accuracy)))
			{
				//printf("Failed lower %.50f < %.50f\n", x, range->low);
				return false;
			}
		}
	}

	// Handle high
	{
		if (fabs(ival->high) == INFINITY)
		{
			if (ival->end_cap == open && ival->high == INFINITY && x == INFINITY)
			{
				return false;
			}

			if (ival->high == -INFINITY && x != -INFINITY)
			{
				return false;
			}
		}
		else
		{
			if ((ival->end_cap == open && xr >= (ival->high * (double)accuracy)) || (ival->end_cap == closed && xr > (ival->high * (double)accuracy)))
			{
				//printf("Failed upper %.50f > %.50f\n", x, range->high);
				return false;
			}
		}
	}

	return true;
#endif

#if 0
	// Hangle low
	{
		if (fabs(ival->low) == INFINITY)
		{
			if (ival->begin_cap == open && ival->low == -INFINITY && x == -INFINITY)
			{
				return false;
			}

			if (ival->low == INFINITY && x != INFINITY)
			{
				return false;
			}
		}
		else
		{
			if ((ival->begin_cap == open && x <= ival->low) || (ival->begin_cap == closed && x < ival->low))
			{
				//printf("Failed lower %.50f < %.50f\n", x, range->low);
				return false;
			}
		}
	}

	// Handle high
	{
		if (fabs(ival->high) == INFINITY)
		{
			if (ival->end_cap == open && ival->high == INFINITY && x == INFINITY)
			{
				return false;
			}

			if (ival->high == -INFINITY && x != -INFINITY)
			{
				return false;
			}
		}
		else
		{
			if ((ival->end_cap == open && x >= ival->high) || (ival->end_cap == closed && x > ival->high))
			{
				//printf("Failed upper %.50f > %.50f\n", x, range->high);
				return false;
			}
		}
	}

	return true;
#endif
}
*/

bool interval_overlap(const interval_t * a, const interval_t * b)
{
	// Check malformed
	if (!interval_isvalid(a) || !interval_isvalid(b))
		return false;

	// Check if range is empty
	if (interval_isempty(a) || interval_isempty(b))
		return false;

	// Check if range is everything
	if (interval_iseverything(a) || interval_iseverything(b))
		return true;

	// Check if endpoints touch
	{
		if (a->begin_cap == closed && b->end_cap == closed && a->begin == b->end)
			return true;
		if (b->begin_cap == closed && a->end_cap == closed && b->begin == a->end)
			return true;
	}

	if (a->begin <= a->end && b->begin <= b->end)
	{
		// Both well-formed ranges

		// Order a and b
		if (b->begin < a->begin)
			swap(a, b);

		// Now a.begin < b.begin

		// Handle touching open endpoints
		if (a->end == b->begin && (a->end_cap == open || b->begin_cap == open))
			return false;

		return max(a->end, b->end) - min(a->begin, b->begin) <= (a->end - a->begin) + (b->end - b->begin);
	}
	else if (a->begin <= a->end || b->begin <= b->end)
	{
		// One well-formed range, one looping through infinity

		// Order a and b
		if ((b->end - b->begin) < 0)
			swap(a, b);

		// Now a is looping through inf, b is well-formed

		// Check if fully encapsulated
		if ((b->begin < a->end && b->end < a->end) || (b->begin > a->begin && b->end > a->begin))
			return true;

		// Check if partially encapsulated
		if ((b->begin < a->begin && b->end > a->begin) || (b->begin < a->end && b->end > a->end))
			return true;

		return false;
	}
	else
	{
		// Both range looping through infinity
		return true;
	}
}

int interval_snprintf(const interval_t * ival, char * str, size_t n)
{
	if (ival->begin == ival->end)	return snprintf(str, n, "%c%f%c", (ival->begin_cap == closed? '[' : '('), ival->begin, (ival->end_cap == closed? ']' : ')'));
	else							return snprintf(str, n, "%c%f, %f%c", (ival->begin_cap == closed? '[' : '('), ival->begin, ival->end, (ival->end_cap == closed? ']' : ')'));
}
