#include <stdio.h>

#include <fraction.h>

#define min(a,b) ({ typeof(a) __a = (a); typeof(b) __b = (b); (__a < __b)? __a : __b; })
#define max(a,b) ({ typeof(a) __a = (a); typeof(b) __b = (b); (__a > __b)? __a : __b; })


unsigned FRACTYPE frac_gcd(const fraction_t * frac)
{
	// Error checking
	{
		if (frac == NULL) return 0;
	}

	if (frac->num == 0) return 0;

	/* //Method 1 (unused)
	unsigned FRACTYPE small = min(abs(frac->num), frac->denom), big = max(abs(frac->num), frac->denom);
	while (true)
	{
		big %= small;	if (big == 0) return small;
		small %= big;	if (small == 0) return big;
	} */

	// Method 2
	unsigned FRACTYPE a = FTABS(frac->num), b = frac->denom;
	while (a != 0)
	{
		unsigned FRACTYPE c = a;
		a = b % a;
		b = c;
	}
	return b;
}

void frac_reduce(fraction_t * frac)
{
	// Error checking
	{
		if (frac == NULL) return;
	}

	unsigned FRACTYPE gcm = frac_gcd(frac);
	if (gcm > 1)
	{
		frac->num /= (signed FRACTYPE)gcm;
		frac->denom /= gcm;
	}
	else if (gcm == 0)
	{
		frac->denom = 1;
	}
}

fraction_t frac_add(fraction_t left, fraction_t right)
{
	// Handle NaN
	if (frac_isnan(&left) || frac_isnan(&right))
	{
		return frac_isnan(&left)? left : right;
	}

	// Handle infinity
	if (frac_isinf(&left) || frac_isinf(&right))
	{
		if (frac_isinf(&left) && frac_isinf(&right))
		{
			if (frac_sgn(&left) != frac_sgn(&right))
				return frac_nan();

			return left;
		}

		return frac_isinf(&left)? left : right;
	}

	return frac_new(left.num * (signed FRACTYPE)right.denom + right.num * (signed FRACTYPE)left.denom, left.denom * right.denom);
}

fraction_t frac_mul(fraction_t left, fraction_t right)
{
	// Handle NaN
	if (frac_isnan(&left) || frac_isnan(&right))
	{
		return frac_isnan(&left)? left : right;
	}

	// Handle infinity
	if (frac_isinf(&left) || frac_isinf(&right))
	{
		signed FRACTYPE sign = frac_sgn(&left) * frac_sgn(&right);
		return (fraction_t){.num = sign * FTMAX, .denom = 1};
	}

	return frac_new(left.num * right.num, left.denom * right.denom);
}

fraction_t frac_div(fraction_t top, fraction_t bottom)
{
	// Handle NaN
	if (frac_isnan(&top) || frac_isnan(&bottom))
	{
		return frac_isnan(&top)? top : bottom;
	}

	// Handle infinity
	if (frac_isinf(&top) || frac_isinf(&bottom))
	{
		if (frac_isinf(&top) && frac_isinf(&bottom))
			return frac_nan();

		if (frac_isinf(&bottom))
			return frac_zero();

		signed FRACTYPE sign = frac_sgn(&bottom);
		return frac_mul((fraction_t){.num = sign, .denom = 1}, frac_inf());
	}

	return frac_mul(top, frac_recip(bottom));
}

int frac_snprintf(const fraction_t * frac, char * str, size_t n)
{
	if (frac_isnan(frac))					return snprintf(str, n, "nan");
	else if (frac_iswhole(frac))			return snprintf(str, n, FTPRId, frac->num);
	else if (frac_isinf(frac))				return snprintf(str, n, "%sinf", frac_sgn(frac) < 0? "-" : "");
	else if (frac_denominator(frac) == 1)	return snprintf(str, n, FTPRId, frac->num);
	else									return snprintf(str, n, FTPRId "/" FTPRIu, frac->num, frac->denom);
}
