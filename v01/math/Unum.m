(* ::Package:: *)

BeginPackage["Unum`"];


GenUnums[ndec_Integer /; ndec >= 1, nbits_Integer /; nbits >= 3] := Module[
{unum = <|Symbol["ndec"] -> ndec, Symbol["nbits"] -> nbits, ns -> 2^nbits|>, exacts, hset},
exacts = Range[10^(ndec-1), 10^ndec-1]/10^(ndec-1);
exacts = Union[exacts, 10/exacts];
AppendTo[unum, uperdec -> 2 Length[exacts] - 2];

While[Length[exacts] < 2^(nbits-3), exacts = Union[exacts, 10 exacts]];
exacts = Take[exacts, {1, 2^(nbits-3)}];
exacts = Union[exacts, 1/exacts];

AppendTo[unum, {minpos -> exacts[[1]], maxpos -> exacts[[-1]], minneg -> -exacts[[1]], maxneg -> -exacts[[-1]]}];
exacts = Union[exacts, -exacts, {0, ComplexInfinity}];

hset = Append[Table[exacts[[i]], {i, 1, Length[exacts]-1}], "\[PlusMinus]\[Infinity]"];
hset = Join[{Row[{"(-\[Infinity],",hset[[1]],")"}]}, Flatten@Table[{hset[[i]], Row[{"(",hset[[i]],",",hset[[i+1]],")"}]}, {i, 1, Length[exacts]-2}], {hset[[-2]], Row[{"(",hset[[-2]],",\[Infinity])"}],hset[[-1]]}];

AppendTo[unum, {Symbol["exacts"] -> exacts, Symbol["hset"] -> hset}];
unum
];

(* Test functions used during library validation *)
TESTGenUnums[ndec_Integer /; ndec >= 1, nbits_Integer /; nbits >= 3, property_String] := Symbol[property] /. GenUnums[ndec, nbits] //N


GenUnums::usage = "Generate unum list";
EndPackage[];
