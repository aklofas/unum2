#include <iostream>
#include <fstream>
#include <math.h>

#include <Unum.hpp>

template<typename T> T sin(T x) {
	return x - (x*x*x)/6.0 + (x*x*x*x*x)/120.0 - (x*x*x*x*x*x*x)/5040.0;
}

template<typename T> T cos(T x) {
	return 1.0 - (x*x)/2.0 + (x*x*x*x)/24.0 - (x*x*x*x*x*x)/720.0;
}

template<typename T> T exp(T x) {
	return 1.0 + x + (x*x)/2.0 + (x*x*x)/6.0 + (x*x*x*x)/24.0;
}



int main(int argc, char **argv)
{

	// Analyze sin(x)
	{
		AnalysisSystem<double> as;

		// Numerical integrate
		auto accum = as.convert(0);
		double dx = 0.01;
		for (double x = 0; x < M_PI; x += dx) {
			accum += sin(as.convert(x)) * dx;
		}

		std::ofstream("analysis-sin.csv") << as;
	}


	// Analyze cos(x)
	{
		AnalysisSystem<double> as;

		// Numerical integrate
		auto accum = as.convert(0);
		double dx = 0.01;
		for (double x = 0; x < M_PI; x += dx) {
			accum += cos(as.convert(x)) * dx;
		}

		std::ofstream("analysis-cos.csv") << as;
	}

	// Analyze exp(x)
	{
		AnalysisSystem<double> as;

		// Numerical integrate
		auto accum = as.convert(0);
		double dx = 0.01;
		for (double x = 0; x < 1.0; x += dx) {
			accum += exp(as.convert(x)) * dx;
		}

		std::ofstream("analysis-exp.csv") << as;
	}

	// Analyze exp(-x)
	{
		AnalysisSystem<double> as;

		// Numerical integrate
		auto accum = as.convert(0);
		double dx = 0.01;
		for (double x = 0; x < 1.0; x += dx) {
			accum += exp(-1.0*as.convert(x)) * dx;
		}

		std::ofstream("analysis-expneg.csv") << as;
	}

	return 0;
}
