#ifndef __UNUM_HPP
#define __UNUM_HPP

#define DEBUG

#include <memory>
#include <utility>
#include <ostream>

template<typename B, typename N> class NumberSystem {
	protected:
		std::shared_ptr<B> system;
		NumberSystem(std::shared_ptr<B> system) : system(system) {}
		NumberSystem() : system(nullptr) {}
		NumberSystem(NumberSystem&& other) : system(std::move(other.system)) {}

	public:
		virtual ~NumberSystem() {}
		virtual N convert(double num) = 0;
};

template<typename B> class Number {
	protected:
		const std::shared_ptr<B> backing;
		Number(const std::shared_ptr<B>& backing) : backing(backing) {}

//public:


};


// Analysis
#include <map>

template<typename T> class NumberImpl;

template<typename T> class AnalysisSystem : NumberSystem<std::map<T, unsigned int>, NumberImpl<T>>
{
public:
	AnalysisSystem() : NumberSystem<std::map<T, unsigned int>, NumberImpl<T>>(std::shared_ptr<std::map<T, unsigned int>>(new std::map<T, unsigned int>)) {}
	void addNumber(T number, unsigned int weight = 1) { (*this->system)[number] += weight; }

	//std::map<T, unsigned int>& getMap() { return *(this->system); }
	//std::map<T, unsigned int>::iterator& getMapIterator() { return this->system->begin(); }

	NumberImpl<T> convert(double num) { return NumberImpl<T>(this->system, num); }

	friend std::ostream& operator<<(std::ostream& os, const AnalysisSystem<double>& as);
};

template<typename T> class NumberImpl : Number<std::map<T, unsigned int>>
{
private:
	T num;

protected:
	void addNumber(T number, unsigned int weight = 1) { (*this->backing)[number] += weight; }
	NumberImpl(const std::shared_ptr<std::map<T, unsigned int>>& backing, T number, unsigned int weight = 1) : Number<std::map<T, unsigned int>>(backing), num(number) { addNumber(number, weight); };

public:
	NumberImpl(const NumberImpl& other) = default;
	NumberImpl(NumberImpl&& other) = default;
	virtual ~NumberImpl() = default;

	void operator=(NumberImpl<T> other) { num = other.num; addNumber(num);  }
	void operator+=(NumberImpl<T> other) { num += other.num; addNumber(num); }
	void operator-=(NumberImpl<T> other) { num -= other.num; addNumber(num); }
	void operator*=(NumberImpl<T> other) { num *= other.num; addNumber(num); }
	void operator/=(NumberImpl<T> other) { num /= other.num; addNumber(num); }

	inline friend NumberImpl<T> operator+(const NumberImpl<T>& left, const NumberImpl<T>& right) { return NumberImpl<T>(left.backing, left.num + right.num); }
	inline friend NumberImpl<T> operator-(const NumberImpl<T>& left, const NumberImpl<T>& right) { return NumberImpl<T>(left.backing, left.num - right.num); }
	inline friend NumberImpl<T> operator*(const NumberImpl<T>& left, const NumberImpl<T>& right) { return NumberImpl<T>(left.backing, left.num * right.num); }
	inline friend NumberImpl<T> operator/(const NumberImpl<T>& left, const NumberImpl<T>& right) { return NumberImpl<T>(left.backing, left.num / right.num); }

	inline friend NumberImpl<T> operator+(const NumberImpl<T>& left, const T& right) { return left + NumberImpl<T>(left.backing, right); }
	inline friend NumberImpl<T> operator-(const NumberImpl<T>& left, const T& right) { return left - NumberImpl<T>(left.backing, right); }
	inline friend NumberImpl<T> operator*(const NumberImpl<T>& left, const T& right) { return left * NumberImpl<T>(left.backing, right); }
	inline friend NumberImpl<T> operator/(const NumberImpl<T>& left, const T& right) { return left / NumberImpl<T>(left.backing, right); }

	inline friend NumberImpl<T> operator+(const T& left, const NumberImpl<T>& right) { return NumberImpl<T>(right.backing, left) + right; }
	inline friend NumberImpl<T> operator-(const T& left, const NumberImpl<T>& right) { return NumberImpl<T>(right.backing, left) - right; }
	inline friend NumberImpl<T> operator*(const T& left, const NumberImpl<T>& right) { return NumberImpl<T>(right.backing, left) * right; }
	inline friend NumberImpl<T> operator/(const T& left, const NumberImpl<T>& right) { return NumberImpl<T>(right.backing, left) / right; }


	friend NumberImpl<T> AnalysisSystem<T>::convert(double num);

	friend std::ostream& operator<<(std::ostream& os, const NumberImpl<T>& num) { os << num.num; return os; }
};


// Unums
#include <unum.h>

class Sorn;


/**
 * Class UnumSystem
 */
class UnumSystem : NumberSystem<unumd_t, Sorn>
{
	public:
		UnumSystem(unsigned int udec, unsigned int ubits);
		UnumSystem(const UnumSystem& other) = delete;
		~UnumSystem() = default;
		//UnumSystem(UnumSystem&& other) : system(std::move(other.system)) {}
		UnumSystem& operator=(const UnumSystem& other) { system = other.system; return *this; }
		UnumSystem& operator=(UnumSystem&& other) { system = std::move(other.system); return *this; }

		Sorn convert(signed FRACTYPE num);
		Sorn convert(signed FRACTYPE low, signed FRACTYPE high);
		Sorn convert(double num);
		Sorn convert(double low, double high);
		Sorn convert(const fraction_t& frac);
		Sorn convert(const fraction_t& low, const fraction_t& high);

#ifdef DEBUG
		friend std::ostream& operator<<(std::ostream& os, const UnumSystem& us);
#endif
};


/**
 * Class Sorn
 */
class Sorn : Number<unumd_t>
{
	protected:
		sorn_t sorn;

		Sorn(const std::shared_ptr<unumd_t>& backing, unum_t low, unum_t high) : Number(backing), sorn(sorn_new(backing.get(), low, high, 0x0)) {};
		Sorn(const std::shared_ptr<unumd_t>& backing, sorn_t sorn) : Number(backing), sorn(sorn) {};

	public:
		Sorn(const Sorn& other) = default;
		Sorn(Sorn&& other) = default;
		virtual ~Sorn() = default;

		friend Sorn UnumSystem::convert(signed FRACTYPE num);
		friend Sorn UnumSystem::convert(signed FRACTYPE low, signed FRACTYPE high);
		friend Sorn UnumSystem::convert(double num);
		friend Sorn UnumSystem::convert(double low, double high);
		friend Sorn UnumSystem::convert(const fraction_t& frac);
		friend Sorn UnumSystem::convert(const fraction_t& low, const fraction_t& high);

		inline friend Sorn operator+(const Sorn& left, const Sorn& right) { return Sorn(left.backing, sorn_add(left.backing.get(), left.sorn, right.sorn)); }
		inline friend Sorn operator-(const Sorn& left, const Sorn& right) { return Sorn(left.backing, sorn_sub(left.backing.get(), left.sorn, right.sorn)); }
		inline friend Sorn operator*(const Sorn& left, const Sorn& right) { return Sorn(left.backing, sorn_mul(left.backing.get(), left.sorn, right.sorn)); }
		inline friend Sorn operator/(const Sorn& left, const Sorn& right) { return Sorn(left.backing, sorn_div(left.backing.get(), left.sorn, right.sorn)); }

		inline bool isempty() const { return sorn_isempty(backing.get(), &sorn); }
		inline bool iseverything() const { return sorn_isempty(backing.get(), &sorn); }
		inline bool isimporper() const { return sorn_isimporper(backing.get(), &sorn); }

		inline interval_t toInterval() const { return sorn2interval(backing.get(), sorn); }
		explicit operator interval_t() const { return toInterval(); }
};


std::ostream& operator<<(std::ostream& os, const Sorn& sorn);
std::ostream& operator<<(std::ostream& os, const interval_t& range);
std::ostream& operator<<(std::ostream& os, const fraction_t& frac);

#endif
