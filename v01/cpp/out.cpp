#include <limits>

#include <Unum.hpp>

#ifdef DEBUG
std::ostream& operator<<(std::ostream& os, const UnumSystem& us)
{
    os << "UnumSystem(" << us.system.get()->nbits << ")";
    os << std::endl << " [refs=" << us.system.use_count() << "]";
    return os;
}
#endif

std::ostream& operator<<(std::ostream& os, const Sorn& sorn)
{
	os << sorn.toInterval();
	return os;
}

std::ostream& operator<<(std::ostream& os, const interval_t& ival)
{
	if (ival.begin == ival.end)	os << (ival.begin_cap == closed? '[' : '(') << ival.begin << (ival.end_cap == closed? ']' : ')');
	else							os << (ival.begin_cap == closed? '[' : '(') << ival.begin << ',' << ival.end << (ival.end_cap == closed? ']' : ')');
    return os;
}

std::ostream& operator<<(std::ostream& os, const fraction_t& frac)
{
	if (frac_isnan(&frac))				os << "nan";
	else if (frac_iswhole(&frac))		os << frac_numerator(&frac);
	else if (frac_isinf(&frac))			os << (frac_sgn(&frac) < 0? "-" : "") << "inf";
	else if (frac_denominator(&frac))	os << frac_numerator(&frac);
	else								os << frac_numerator(&frac) << '/' << frac_denominator(&frac);
	return os;
}

std::ostream& operator<<(std::ostream& os, const AnalysisSystem<double>& as)
{
	os.precision(std::numeric_limits<double>::max_digits10);

	auto map = *as.system;
	for(auto iter = map.begin(); iter != map.end(); ++iter)
	{
		os << std::fixed << iter->first << "," << iter->second << std::endl;
	}

	return os;
}
