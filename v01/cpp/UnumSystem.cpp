#include <new>
#include "Unum.hpp"


UnumSystem::UnumSystem(unsigned int udec, unsigned int ubits)
{
	unumd_t * udt = unum_new(udec, ubits);

	if (udt == NULL)
		throw std::bad_alloc();

	system = std::shared_ptr<unumd_t>(udt, [](unumd_t * udt) { unum_free(udt); });
}

#if 0
// No copy!
UnumSystem::UnumSystem(const UnumSystem& ous)
{
	unumd_t * udt = unum_copy(ous.ud.get());

	if (udt == NULL)
		throw std::bad_alloc();

	ud = std::shared_ptr<unumd_t>(udt, [](unumd_t * udt) { unum_free(udt); });
}
#endif

#if 0
UnumSystem& UnumSystem::operator=(UnumSystem us)
{
	swap(us);
	return *this;
}
#endif

Sorn UnumSystem::convert(signed FRACTYPE num)
{
	return convert(frac_new(num, 1));
}

Sorn UnumSystem::convert(signed FRACTYPE low, signed FRACTYPE high)
{
	return convert(frac_new(low, 1), frac_new(high, 1));
}

Sorn UnumSystem::convert(double num)
{
	return convert(num, num);
}

Sorn UnumSystem::convert(double low, double high)
{
	return Sorn(system, doubles2sorn(system.get(), low, high));
}

Sorn UnumSystem::convert(const fraction_t& frac)
{
	return convert(frac, frac);
}

Sorn UnumSystem::convert(const fraction_t& low, const fraction_t& high)
{
	return Sorn(system, fracs2sorn(system.get(), low, high));
}
