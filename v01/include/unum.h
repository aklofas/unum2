#ifndef __UNUM_H
#define __UNUM_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <fraction.h>
#include <interval.h>

#ifdef __cplusplus
extern "C" {
#endif

#define _FRACTIONS	1
#define _DOUBLES 2

#define USE_ADDTBL YES
#define USE_MULTBL YES
#define USE_EXACTS _FRACTIONS
//#define USE_EXACTS _DOUBLES

#define UNUM_BACKTYPE	uint16_t
#define SORN_FLAGTYPE	uint8_t


typedef UNUM_BACKTYPE unum_t;
typedef SORN_FLAGTYPE sornflags_t;

#define SORN_NOFLAGS	(0x0)
#define SORN_EMPTY		(0x1 << 0)

typedef struct __attribute__ ((packed))
{
	sornflags_t flags;
	unum_t low, high;
} sorn_t;
#define sflags(s)	((s).flags)
#define ulow(s)  	((s).low)
#define uhigh(s) 	((s).high)


typedef struct
{
	unsigned int nbits;
	size_t ns;

	unum_t mask;
	unsigned long accuracy;

#if USE_EXACTS == _FRACTIONS
	fraction_t *exactsf, *exactsf_zero, *exactsf_max;

#elif USE_EXACTS == _DOUBLES
	double *exactsd, *exactsd_zero, *exactsd_max;

#else
	#error Unknown exacts type
#endif

#ifdef USE_ADDTBL
	sorn_t * addtable;
#endif

#ifdef USE_MULTBL
	sorn_t * multable;
#endif
} unumd_t;



// Unum functions

//! Creates a new unum2 type system with specified parameters
/*!
 * Returns a heap-allocated unum2-descriptor struct or NULL if not enough memory
 * \param ndec The (minimum) number of decimals of accuracy
 * \param nbits The number if bits in each unum string
 */
unumd_t * unum_new(unsigned int ndec, unsigned int nbits);

//! Creates a full memory duplicate of the given unum2 type system
/*!
 * Returns a heap-allocated full copy of the unum2-descriptor or NULL if not enough memory
 * \param oud The unum2 type system to duplicate
 */
unumd_t * unum_copy(const unumd_t * oud);

//! Deallocates the unum2 type system
/*!
 * Safe to call on partially-allocated or NULL objects
 * \param ud The unum2 type descriptor to free
 */
void unum_free(unumd_t * ud);

#define _unum_magbitmask(ud)	((ud)->mask >> 1)
#define _unum_signbitmask(ud)	((ud)->mask ^ _unum_magbitmask(ud))
#define _unum_ubitmask(ud)		(0x1)
#define _unum_cast(ud, n) 		(unum_t)((n) & (ud)->mask)

static inline unum_t unum_pminf(const unumd_t * ud)
{
	return _unum_signbitmask(ud);
}

//! Returns true if specified unum is exact
/*!
 * \param ud Unum2 type descriptor
 * \param n The unum to test
 */
static inline bool unum_isexact(const unumd_t * ud, unum_t n)
{
	(void)ud;
	return !(n & _unum_ubitmask(ud));
}

//! Returns true if specified unum is identically zero
/*!
 * \param ud Unum2 type descriptor
 * \param n The unum to test
 */
static inline bool unum_iszero(const unumd_t * ud, unum_t n)
{
	(void)ud;
	return n == 0;
}

//! Returns true if specified unum is +/- infinity
/*!
 * \param ud Unum2 type descriptor
 * \param n The unum to test
 */
static inline bool unum_ispminf(const unumd_t * ud, unum_t n)
{
	return n == unum_pminf(ud);
}

//! Returns true if specified unum is negative
/*!
 * Returns true if specified unum is between (but not including) +/- infinity and zero
 * \param ud Unum2 type descriptor
 * \param n The unum to test
 */
static inline bool unum_isnegative(const unumd_t * ud, unum_t n)
{
	return !unum_ispminf(ud, n) && (n & _unum_signbitmask(ud));
}

//! Returns the sign of the specified unum
/*!
 * Returns -1 if n < 0, 0 if n == 0, +1 if n > 0. +/- infinity is considered positive
 * \param ud Unum2 type descriptor
 * \param n The unum to inspect the sign bit
 */
static inline int unum_sgn(const unumd_t * ud, unum_t n)
{
	return unum_iszero(ud, n)? 0 : unum_isnegative(ud, n)? -1 : 1;
}

//! Returns the negative of the specified unum
/*!
 * \param ud Unum2 type descriptor
 * \param n The unum to negate
 */
static inline unum_t unum_neg(const unumd_t * ud, unum_t n)
{
	return _unum_cast(ud, -n);
}

//! Returns the reciprocal of the specified unum
/*!
 * \param ud Unum2 type descriptor
 * \param n The unum to reciprocate
 */
static inline unum_t unum_recip(const unumd_t * ud, unum_t n)
{
	return _unum_cast(ud, ((~n & _unum_magbitmask(ud)) | (n & _unum_signbitmask(ud))) + 1);
}

//! Returns the absolute value unum of the specified unum
/*!
 * \param ud Unum2 type descriptor
 * \param n The unum to abs
 */
static inline unum_t unum_abs(const unumd_t * ud, unum_t n)
{
	return unum_isnegative(ud, n)? unum_neg(ud, n) : n;
}


// Sorn functions

static inline sorn_t sorn_new(const unumd_t * ud, unum_t low, unum_t high, sornflags_t flags)
{
	return (sorn_t){.flags = flags, .low = _unum_cast(ud, low), .high = _unum_cast(ud, high)};
}

#define _sorn_testfbit(s, bit)		((sflags(s) & bit) != 0)

static inline sorn_t sorn_empty(const unumd_t * ud)
{
	return sorn_new(ud, 0, 0, SORN_EMPTY);
}

static inline sorn_t sorn_everything(const unumd_t * ud)
{
	return sorn_new(ud, 0, _unum_cast(ud, -1), SORN_NOFLAGS);
}

static inline bool sorn_isempty(const unumd_t * ud, const sorn_t * s)
{
	(void)ud;
	return _sorn_testfbit(*s, SORN_EMPTY);
}

static inline bool sorn_iseverything(const unumd_t * ud, const sorn_t * s)
{
	if (sorn_isempty(ud, s))
		return false;

	return _unum_cast(ud, s->low - 1) == s->high;
}

static inline bool sorn_isimporper(const unumd_t * ud, const sorn_t * s)
{
	if (sorn_isempty(ud, s))
		return false;

	return (_unum_signbitmask(ud) & ulow(*s)) == 0 && (_unum_signbitmask(ud) & uhigh(*s)) != 0;
}

//! Converts the specified unum to a sorn struct
/*!
 * Uses the single specified unum as both low and high
 * \param ud Unum2 type descriptor
 * \param n The unum to convert
 */
static inline sorn_t unum2sorn(const unumd_t * ud, unum_t n)
{
	return sorn_new(ud, n, n, 0x0);
}

//! Converts the specified unums to a sorn struct
/*!
 * \param ud Unum2 type descriptor
 * \param low The lowest unum (using ccw rule) in the contiguous sorn
 * \param high The highest unum (using ccw rule) in the contiguous sorn
 */
static inline sorn_t unums2sorn(const unumd_t * ud, unum_t low, unum_t high)
{
	return sorn_new(ud, low, high, 0x0);
}

//! Returns the negative of the specified sorn
/*!
 * \param ud Unum2 type descriptor
 * \param s The sorn to negate
 */
static inline sorn_t sorn_neg(const unumd_t * ud, sorn_t s)
{
	if (sorn_isempty(ud, &s))
		return s;

	return sorn_new(ud, unum_neg(ud, s.high), unum_neg(ud, s.low), s.flags);
}

//! Returns the reciprocal of the specified sorn
/*!
 * \param ud Unum2 type descriptor
 * \param s The sorn to reciprocate
 */
static inline sorn_t sorn_recip(const unumd_t * ud, sorn_t s)
{
	if (sorn_isempty(ud, &s))
		return s;

	return sorn_new(ud, unum_recip(ud, s.high), unum_recip(ud, s.low), s.flags);
}

//! Add the two specified sorn's together
/*!
 * \param ud Unum2 type descriptor
 * \param left The first sorn to add
 * \param right The second sorn to add
 */
sorn_t sorn_add(const unumd_t * ud, sorn_t left, sorn_t right);

//! Subtracts the right sorn from the left (left - right)
/*!
 * \param ud Unum2 type descriptor
 * \param left The sorn to subtract from
 * \param right The sorn amount to subtract
 */
static inline sorn_t sorn_sub(const unumd_t * ud, sorn_t left, sorn_t right)
{
	return sorn_add(ud, left, sorn_neg(ud, right));
}

//! Multiply the two specified sorn's together
/*!
 * \param ud Unum2 type descriptor
 * \param left The first sorn to multiply
 * \param right The second sorn to multiply
 */
sorn_t sorn_mul(const unumd_t * ud, sorn_t left, sorn_t right);

//! Divides the top sorn into the bottom (top / bottom)
/*!
 * \param ud Unum2 type descriptor
 * \param top The top sorn divide
 * \param bottom The bottom sorn to divide
 */
static inline sorn_t sorn_div(const unumd_t * ud, sorn_t top, sorn_t bottom)
{
	return sorn_mul(ud, top, sorn_recip(ud, bottom));
}


//! Add the two specified unum's together
/*!
 * Shortcut for sorn_add
 * \param ud Unum2 type descriptor
 * \param left The first unum to add
 * \param right The second unum to add
 */
static inline sorn_t unum_add(const unumd_t * ud, unum_t left, unum_t right)
{
	return sorn_add(ud, unum2sorn(ud, left), unum2sorn(ud, right));
}

//! Subtracts the right unum from the left (left - right)
/*!
 * Shortcut for sorn_sub
 * \param ud Unum2 type descriptor
 * \param left The unum to subtract from
 * \param right The unum amount to subtract
 */
static inline sorn_t unum_sub(const unumd_t * ud, unum_t left, unum_t right)
{
	return unum_add(ud, left, unum_neg(ud, right));
}

//! Multiply the two specified unum's together
/*!
 * Shortcut for sorn_mul
 * \param ud Unum2 type descriptor
 * \param left The first unum to multiply
 * \param right The second unum to multiply
 */
static inline sorn_t unum_mul(const unumd_t * ud, unum_t left, unum_t right)
{
	return sorn_mul(ud, unum2sorn(ud, left), unum2sorn(ud, right));
}

//! Divides the top unum into the bottom (top / bottom)
/*!
 * Shortcut for sorn_div
 * \param ud Unum2 type descriptor
 * \param top The top unum divide
 * \param bottom The bottom unum to divide
 */
static inline sorn_t unum_div(const unumd_t * ud, unum_t top, unum_t bottom)
{
	return unum_mul(ud, top, unum_recip(ud, bottom));
}


// Conversion functions

//! Returns the specified fraction in a unum notation
/*!
 * \param ud Unum2 type descriptor
 * \param frac The fraction to convert
 */
unum_t frac2unum(const unumd_t * ud, fraction_t frac);

//! Converts the specified fractions to a sorn struct
/*!
 * First converts the fractions to unum representations, then populates the sorn struct
 * \param ud Unum2 type descriptor
 * \param low The lowest fraction (using ccw rule) in the contiguous sorn
 * \param high The highest fraction (using ccw rule) in the contiguous sorn
 */
static inline sorn_t fracs2sorn(const unumd_t * ud, const fraction_t low, const fraction_t high)
{
	return unums2sorn(ud, frac2unum(ud, low), frac2unum(ud, high));
}

//! Converts the specified fraction to a sorn struct
/*!
 * First converts the fraction to unum representation, then populates the sorn struct
 * \param ud Unum2 type descriptor
 * \param frac The fraction to convert
 */
static inline sorn_t frac2sorn(const unumd_t * ud, const fraction_t frac)
{
	return unum2sorn(ud, frac2unum(ud, frac));
}


//! Returns the specified double in a unum notation
/*!
 * \param ud Unum2 type descriptor
 * \param x The double to convert
 */
unum_t double2unum(const unumd_t * ud, double x);

//! Converts the specified doubles to a sorn struct
/*!
 * First converts the doubles to unum representations, then populates the sorn struct
 * \param ud Unum2 type descriptor
 * \param low The lowest double (using ccw rule) in the contiguous sorn
 * \param high The highest double (using ccw rule) in the contiguous sorn
 */
static inline sorn_t doubles2sorn(const unumd_t * ud, double low, double high)
{
	return unums2sorn(ud, double2unum(ud, low), double2unum(ud, high));
}

//! Returns the interval representation of the given unum
/*!
 * \param ud Unum2 type descriptor
 * \param n The unum to convert
 */
interval_t unum2interval(const unumd_t * ud, unum_t n);

//! Returns the interval representation of the given sorn
/*!
 * \param ud Unum2 type descriptor
 * \param s The sorn to convert
 */
interval_t sorn2interval(const unumd_t * ud, sorn_t s);


#ifdef __cplusplus
}
#endif
#endif
