#ifndef __FRACTION_H
#define __FRACTION_H

#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>
#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif

#define FBITS	32
#if FBITS == 32
	#define FRACTYPE	long int
	#define FTABS		labs
	#define FTRND		lrint
	#define FTMAX		LONG_MAX
	#define FTPRId		"%ld"
	#define FTPRIu		"%lu"
#elif FBITS == 64
	#define FRACTYPE	long long int
	#define FTABS		llabs
	#define FTRND		llrint
	#define FTMAX		LLONG_MAX
	#define FTPRId		"%lld"
	#define FTPRIu		"%llu"
#else
	#error Unknown fraction bits type
#endif


/*!
 * \file fraction.h
 * \brief Minimal fraction library. Handles improper fractions, infinity, and NaNs
 */


//! Fraction structure used throughout fraction library
typedef struct
{
	signed FRACTYPE num;		//!< Numerator of fraction (Signed)
	unsigned FRACTYPE denom;	//!< Denominator of fraction (Unsigned)
} fraction_t;


//! Returns the greatest common denominator of the given fraction
/*!
 * Internally used when reducing a fraction (No need to call by user)
 * \param frac
 */
unsigned FRACTYPE frac_gcd(const fraction_t * frac);

//! Reduces the given fraction if possible
/*!
 * Internally called when manipulating fractions. (Should never need to call directly)
 * \param frac The fraction to reduce
 */
void frac_reduce(fraction_t * frac);


// Ctors

//! Constructs a new fraction (reduces automatically)
/*!
 * \param num Fraction numerator, can be negative
 * \param denom Fraction denominator, must be positive
 */
static inline fraction_t frac_new(signed FRACTYPE num, unsigned FRACTYPE denom)
{
	fraction_t frac = {.num = num, .denom = denom};
	frac_reduce(&frac);

	return frac;
}

//! Copies the given fraction, returns a new instance by value
static inline fraction_t frac_copy(const fraction_t * frac)
{
	return *frac;
}


// Getters

//! Returns the fractions numerator (getter fn)
static inline signed FRACTYPE frac_numerator(const fraction_t * frac)
{
	return frac->num;
}

//! Returns the fractions denominator (getter fn)
static inline unsigned FRACTYPE frac_denominator(const fraction_t * frac)
{
	return frac->denom;
}

//#define num(f) frac_numerator(&f)
//#define denom(f) frac_denominator(&f)


// Constant functions

//! Returns a fraction representing zero
static inline fraction_t frac_zero()
{
	return (fraction_t){.num = 0, .denom = 1};
}

//! Returns a fraction representing positive infinity
static inline fraction_t frac_inf()
{
	return (fraction_t){.num = FTMAX, .denom = 1};
}

//! Returns a fraction representing NaN
static inline fraction_t frac_nan() {
	return (fraction_t){.num = 1, .denom = 0};
}


//! Returns true if given fraction is zero
/*!
 * \param frac The fraction is compare to zero
 */
static inline bool frac_iszero(const fraction_t * frac)
{
	return frac->num == 0 && frac->denom != 0;
}

//! Returns true if fraction represents a whole number
/*!
 * \param frac The fraction to test if whole number
 */
static inline bool frac_iswhole(const fraction_t * frac)
{
	return frac->denom == 1;
}

//! Returns true if given fraction is positive or negative infinity
/*!
 * \param frac The fraction is compare to +-inf
 */
static inline bool frac_isinf(const fraction_t * frac)
{
	return FTABS(frac->num) == FTMAX && frac->denom == 1;
}

//! Returns true if given fraction is NaN
/*!
 * \param frac The fraction to test if NaN
 */
static inline bool frac_isnan(const fraction_t * frac)
{
	return frac->denom == 0;
}


// Math functions

//! Returns a negative instance of the given fraction
/*!
 * If frac is NaN, returns frac parameter (no change)
 * \param frac The fraction to negate
 */
static inline fraction_t frac_neg(fraction_t frac)
{
	if (frac_isnan(&frac))
		return frac;

	return (fraction_t){.num = -frac.num, .denom = frac.denom};
}

//! Returns the absolute value instance of the given fraction
/*!
 * If frac is NaN, returns frac prameter (no change)
 * \param frac The fraction to abs
 */
static inline fraction_t frac_abs(fraction_t frac)
{
	if (frac_isnan(&frac))
		return frac;

	return (fraction_t){.num = FTABS(frac.num), .denom = frac.denom};
}

//! Returns the sign of the given fraction
/*!
 * (-1 if frac < 0, 0 if frac == 0, 1 if frac > 0). If frac is NaN, returns 0
 * \param frac The fraction to inspect sign
 */
static inline signed FRACTYPE frac_sgn(const fraction_t * frac)
{
	if (frac_isnan(frac))
		return 0;

	return (frac->num > 0) - (frac->num < 0);
}

//! Adds two fractions together, returns new fraction instance
/*!
 * \param left First fraction
 * \param right Second fraction
 */
fraction_t frac_add(fraction_t left, fraction_t right);

//! Subtracts two fractions (left - right), returns new fraction instance
/*!
 * \param left First fraction
 * \param right Second fraction
 */
static inline fraction_t frac_sub(fraction_t left, fraction_t right)
{
	return frac_add(left, frac_neg(right));
}

//! Multiplies two fractions together, returns new fraction instance
/*!
 * \param left First fraction
 * \param right Second fraction
 */
fraction_t frac_mul(fraction_t left, fraction_t right);

//! Divides two fractions (top / bottom)
/*!
 * \param top First fraction
 * \param bottom Second fraction
 */
fraction_t frac_div(fraction_t top, fraction_t bottom);

//! Reciprocates the given fraction, returns new fraction instance
/*!
 * Handles infinity and NaN's properly [1/(+-inf) = 0]
 * \param frac Fraction to reciprocate
 */
static inline fraction_t frac_recip(fraction_t frac)
{
	// Handle Nan
	if (frac_isnan(&frac))
		return frac;

	// Handle infinity and zero
	if (frac_isinf(&frac) || frac_iszero(&frac))
		return frac_zero();

	return (fraction_t){.num = frac_sgn(&frac) * (signed FRACTYPE)frac.denom, .denom = (unsigned FRACTYPE)FTABS(frac.num)};
}


// Comparison functions

//! Returns true if both fractions are equal
/*!
 * Equality with NaN's is always false
 * \param a Test equality
 * \param b Test equality
 */
static inline bool frac_eq(const fraction_t * a, const fraction_t * b)
{
	// Handle NaN
	if (frac_isnan(a) || frac_isnan(b))
		return false;

	return a->num == b->num && a->denom == b->denom;
}

//! Returns true is fractions are not equal
/*!
 * Equality with NaN's is always false
 * \param a Test non-equality
 * \param b Test non-equality
 */
static inline bool frac_neq(const fraction_t * a, const fraction_t * b)
{
	return !frac_eq(a, b);
}

//! Returns true if fractions a < b
/*!
 * Comparison with NaN's is always false
 * \param a Test a < b
 * \param b Test a < b
 */
static inline bool frac_lt(const fraction_t * a, const fraction_t * b)
{
	// Handle NaN
	if (frac_isnan(a) || frac_isnan(b))
		return false;

	return a->num * (signed FRACTYPE)b->denom < b->num * (signed FRACTYPE)a->denom;
}

//! Returns true if fractions a <= b
/*!
 * Comparison with NaN's is always false
 * \param a Test a <= b
 * \param b Test a <= b
 */
static inline bool frac_leq(const fraction_t * a, const fraction_t * b)
{
	// Handle NaN
	if (frac_isnan(a) || frac_isnan(b))
		return false;

	return a->num * (signed FRACTYPE)b->denom <= b->num * (signed FRACTYPE)a->denom;
}

//! Returns true if fractions a > b
/*!
 * Comparison with NaN's is always false
 * \param a Test a > b
 * \param b Test a > b
 */
static inline bool frac_gt(const fraction_t * a, const fraction_t * b)
{
	// Handle NaN
	if (frac_isnan(a) || frac_isnan(b))
		return false;

	return !frac_leq(a, b);
}

//! Returns true if fractions a >= b
/*!
 * Comparison with NaN's is always false
 * \param a Test a >= b
 * \param b Test a >= b
 */
static inline bool frac_geq(const fraction_t * a, const fraction_t * b)
{
	// Handle NaN
	if (frac_isnan(a) || frac_isnan(b))
		return false;

	return !frac_lt(a, b);
}


// Conversion functions

//! Converts a given double to a fraction to within 1/accuracy
/*!
 * Handles +-infinity and NaN's
 * \param x The double to rationalize
 * \param accuracy Rationalize up to specific accuracy
 */
static inline fraction_t double2frac(double x, FRACTYPE accuracy)
{
	// Handle NaN
	if (isnan(x))
		return frac_nan();

	// Handle infinity
	if (fabs(x) == INFINITY)
	{
		return (x < 0)? frac_neg(frac_inf()) : frac_inf();
	}

	return frac_new((signed FRACTYPE)FTRND(x * (double)accuracy), accuracy);
}

//! Converts given fraction to approximate double precision
/*!
 * Passes NaN and infinity to double's representation if appropriate
 * \param frac The fraction to convert
 */
static inline double frac2double(fraction_t frac)
{
	// Handle NaN
	if (frac_isnan(&frac))
		return NAN;

	// Handle infinity
	if (frac_isinf(&frac))
		return frac_sgn(&frac) * INFINITY;

	return (double)frac.num / (double)frac.denom;
}

// Print functions

//! Prints the given fraction into specified buffer.
/*!
 * Usage is identical to the standard snprintf
 * \param frac The fraction to print
 * \param str The charater buffer to print to
 * \param n Length of str buffer
 */
int frac_snprintf(const fraction_t * frac, char * str, size_t n);


#ifdef __cplusplus
}
#endif
#endif
