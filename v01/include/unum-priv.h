#ifndef __UNUM_PRIV_H
#define __UNUM_PRIV_H

#include <unum.h>

#ifdef __cplusplus
extern "C" {
#endif

/****
 * Private library-only functions
 * *** Potentially DANGEROUS to use outside of library ***
 * (Do not use)
 */


#if USE_EXACTS == _FRACTIONS
	fraction_t unum2exactfrac(const unumd_t * ud, unum_t n);
#endif


double unum2exactdouble(const unumd_t * ud, unum_t n);


#if defined(USE_ADDTBL) || defined(USE_MULTBL)
	#define _lookup_tidx(a,b) ((a*ns)+b)
#endif

sorn_t sorn_doadd(const unumd_t * ud, sorn_t left, sorn_t right);
sorn_t sorn_domul(const unumd_t * ud, sorn_t left, sorn_t right);


#ifdef __cplusplus
}
#endif
#endif
