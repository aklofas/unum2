#ifndef __RANGE_H
#define __RANGE_H

#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif


/*!
 * \file interval.h
 * \brief Minimal interval library. Handles open/closed regular intervals (start < end) as well open/closed improper intervals looping through infinity.
 */

//! Intervals can be either open or closed on each side
typedef enum
{
	closed,
	open
} intervalcap_t;

//! Interval structure used throughout interval library
typedef struct
{
	const double begin, end;
	const intervalcap_t begin_cap, end_cap;
} interval_t;
/*
#define rlow(r)     (r).low
#define rhigh(r)    (r).high
#define rclosed(lh) (( lh ## i ) == closed)
#define ropen(lh)   (( lh ## i ) == open)
*/

//! Constructs and returns a new interval with specified parameters
/*!
 * \param begin_cap Begin point the interval is either open/closed
 * \param begin Begin point of the interval (Low point if interval is regular, High point if interval loops through infinity)
 * \param end End point of the interval (High point if interval is regular, Low point if interval loops through infinity)
 * \param end_cap End point of the interval is either open/closed
 */
static inline interval_t interval_new(intervalcap_t begin_cap, double begin, double end, intervalcap_t end_cap)
{
	return (interval_t)
	{
		.begin = begin, .end = end,
		.begin_cap = begin_cap, .end_cap = end_cap
	};
}

//! Constructs and returns a new regular open interval that ranges +/- epsilon around x
/*!
 * \param x Anchor of the open interval
 * \param epsilon 1/2 width of the open interval
 */
static inline interval_t interval_epsilon(double x, double epsilon)
{
	return interval_new(open, x - fabs(epsilon), x + fabs(epsilon), open);
}

//! Constructs and returns a new regular closed interval that ranges +/- one ULP around x
/*!
 * \param x Anchor of the closed interval
 */
static inline interval_t interval_rangeulp(double x)
{
	return interval_new(closed, nextafter(x, -INFINITY), nextafter(x, INFINITY), closed);
}

//! Constructs and returns a right-open interval starting at begin (with open/closed parameters) and ends at +inf
/*!
 * \param begin_cap Begin point the interval is either open/closed
 * \param begin Begin point of the interval (Low point)
 */
static inline interval_t interval_rightopen(intervalcap_t begin_cap, double begin)
{
	return interval_new(begin_cap, begin, INFINITY, closed);
}

//! Constructs and returns a left-open interval starting at -inf and ending at end (with open/closed parameters)
/*!
 * \param end End point of the interval (High point)
 * \param end_cap End point of the interval is either open/closed
 */
static inline interval_t interval_leftopen(double end, intervalcap_t end_cap)
{
	return interval_new(closed, -INFINITY, end, end_cap);
}

//! Returns an interval representing every known value on the real number line
static inline interval_t interval_everything()
{
	return interval_new(closed, 0.0, 0.0, open);
}

//! Returns true if there is an overlapping region between the two given intervals
/*!
 * \param a The first interval to test if overlapping
 * \param b The second interval to test if overlapping
 */
bool interval_overlap(const interval_t * a, const interval_t * b);

static inline bool interval_isempty(const interval_t * r)
{
	return r->begin == r->end && r->begin_cap == open && r->end_cap == open;
}

//! Returns true if the given interval encompasses all possible values on the real number line (-inf to +inf)
/*!
 * \param ival Interval to test
 */
static inline bool interval_iseverything(const interval_t * ival)
{
	return ival->begin == ival->end && ival->begin_cap == closed && ival->end_cap == open;
}

//! Returns true if given interval is a valid interval
/*!
 * \param ival Interval to test if valid
 */
static inline bool interval_isvalid(const interval_t * ival)
{
	return !(ival->begin == ival->end && ival->begin_cap == open && ival->end_cap == closed);
}

//bool inrange(const interval_t * ival, double x, unsigned long accuracy);


// Print functions

//! Prints the given interval into specified buffer.
/*!
 * Usage is identical to the standard snprintf
 * \param ival The interval to print
 * \param str The charater buffer to print to
 * \param n Length of str buffer
 */
int interval_snprintf(const interval_t * ival, char * str, size_t n);


#ifdef __cplusplus
}
#endif
#endif
