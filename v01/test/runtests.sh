#!/bin/sh

export LD_LIBRARY_PATH=/usr/local/Wolfram/Mathematica/10.3/SystemFiles/Links/WSTP/DeveloperKit/Linux-x86-64/CompilerAdditions

./test_mathematica -linklaunch -linkname 'math -mathlink' -linkprotocol 'sharedmemory'
