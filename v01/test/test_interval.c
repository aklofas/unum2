#include <interval.h>

#include "test_c.h"

START_TEST(test_overlap)
{
#define yesoverlap(a,b)	({ interval_t __a = (a), __b = (b); if (!interval_overlap(&__a, &__b)) { \
	char bufa[50], bufb[50]; interval_snprintf(&__a, bufa, sizeof(bufa)); interval_snprintf(&__b, bufb, sizeof(bufb)); \
	ck_abort_msg("Interval must overlap: %s and %s", bufa, bufb); } \
})

#define nooverlap(a,b)	({ interval_t __a = (a), __b = (b); if (interval_overlap(&__a, &__b)) { \
	char bufa[50], bufb[50]; interval_snprintf(&__a, bufa, sizeof(bufa)); interval_snprintf(&__b, bufb, sizeof(bufb)); \
	ck_abort_msg("Interval must NOT overlap: %s and %s", bufa, bufb); } \
})

	// Check malformed ranges
	{
		nooverlap(interval_new(open, -1, 1, open), interval_new(open, 0, 0, closed));
		nooverlap(interval_new(open, 0, 0, closed), interval_new(open, -1, 1, closed));
	}

	// Check everything-ranges
	{
		yesoverlap(interval_new(closed, 0, 0, open), interval_new(open, -1, 1, open));
		yesoverlap(interval_new(closed, 0, 0, open), interval_new(open, 1, 2, open));
		yesoverlap(interval_new(closed, 0, 0, open), interval_new(closed, 1, 2, open));
		yesoverlap(interval_new(closed, 0, 0, open), interval_new(closed, 1, 2, closed));
		yesoverlap(interval_new(closed, 0, 0, open), interval_new(closed, 1, 1, closed));
		yesoverlap(interval_new(closed, 0, 0, open), interval_new(closed, 0, 0, closed));
		yesoverlap(interval_new(closed, 0, 0, open), interval_new(closed, 0, 0, open));

		nooverlap(interval_new(closed, 0, 0, open), interval_new(open, 1, 1, open));
		nooverlap(interval_new(closed, 0, 0, open), interval_new(open, 0, 0, open));
		nooverlap(interval_new(closed, 0, 0, open), interval_new(open, -1, -1, open));
	}

	// Check no-touching ranges
	{
		nooverlap(interval_new(closed, -1, 1, open), interval_new(open, 1, 2, closed));
		nooverlap(interval_new(open, -1, 1, open), interval_new(open, 1, 2, open));
		nooverlap(interval_new(open, -1, 1, open), interval_new(open, 1, -1, open));
		nooverlap(interval_new(closed, 0, 1, open), interval_new(open, 1, -1, open));
		nooverlap(interval_new(open, 1, -1, open), interval_new(open, -1, 1, open));
		nooverlap(interval_new(open, 1, -1, open), interval_new(closed, 0, 1, open));
	}

	// Check touching ranges
	{
		yesoverlap(interval_new(open, -1, 1, closed), interval_new(closed, 1, 2, open));
		yesoverlap(interval_new(open, 0, 1, closed), interval_new(closed, 1, -1, open));
		yesoverlap(interval_new(closed, 1, -1, closed), interval_new(open, 0, 1, closed));
		yesoverlap(interval_new(open, -1, 1, closed), interval_new(closed, 1, -1, open));
		yesoverlap(interval_new(closed, -1, 1, open), interval_new(open, 1, -1, closed));
		yesoverlap(interval_new(open, 1, -1, closed), interval_new(closed, -1, 1, open));
		yesoverlap(interval_new(closed, -1, 1, open), interval_new(open, 1, -1, closed));
	}

	// Check simple well-formed ranges
	{
		// Near identical
		yesoverlap(interval_new(open, -1, 1, open), interval_new(open, -1, 1, open));
		yesoverlap(interval_new(closed, -1, 1, closed), interval_new(closed, -1, 1, closed));

		yesoverlap(interval_new(closed, -1, 1, closed), interval_new(open, -1, 1, open));
		yesoverlap(interval_new(open, -1, 1, open), interval_new(closed, -1, 1, closed));
		yesoverlap(interval_new(closed, -1, 1, open), interval_new(open, -1, 1, closed));
		yesoverlap(interval_new(open, -1, 1, closed), interval_new(closed, -1, 1, open));
		yesoverlap(interval_new(closed, -1, 1, open), interval_new(closed, -1, 1, open));
		yesoverlap(interval_new(open, -1, 1, closed), interval_new(open, -1, 1, closed));

		yesoverlap(interval_new(closed, -1, 1, open), interval_new(open, -1, 1, open));
		yesoverlap(interval_new(open, -1, 1, closed), interval_new(open, -1, 1, open));
		yesoverlap(interval_new(open, -1, 1, open), interval_new(closed, -1, 1, open));
		yesoverlap(interval_new(open, -1, 1, open), interval_new(open, -1, 1, closed));

		// Near empty
		nooverlap(interval_new(open, -1, 1, open), interval_new(open, -1, -1, open));
		nooverlap(interval_new(open, -1, 1, open), interval_new(open, 1, 1, open));

		// TODO - add more checks
	}

	// TODO - Check well-formed/ill-formed ranges
	{

	}

	// TODO - Check ill-formed ranges
	{

	}

}
END_TEST

void tcase_interval(Suite * s)
{
	TCase * tc_interval= tcase_create("Interval");
	tcase_add_test(tc_interval, test_overlap);

	suite_add_tcase(s, tc_interval);
}
