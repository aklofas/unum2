#include <iostream>

//#define CATCH_CONFIG_MAIN
//#include "catch.hpp"

#include <Unum.hpp>

int main(int argc, char **argv)
{
	UnumSystem us(1, 7);
	{
		auto a = us.convert(frac_new(-3,7), frac_new(3,7));
		//auto a = us.convert(frac_new(-1,2), frac_new(1,2));
		auto b = us.convert(frac_new(5,2));
		std::cout << "A: " << (interval_t)a << std::endl;
		std::cout << "B: " << (interval_t)b << std::endl;
		std::cout << "A + B: " << (a+b).toInterval() << std::endl;
		std::cout << "A - B: " << (a-b).toInterval() << std::endl;
		std::cout << "A * B: " << (a*b).toInterval() << std::endl;
		std::cout << "A / B: " << (a/b).toInterval() << std::endl;

		//std::cout << "A improp: " <<  a.isimporper() << std::endl;
		//std::cout << "B improp: " <<  b.isimporper() << std::endl;
	}

	AnalysisSystem<double> as;
	{
		auto a = as.convert(10.1);
		auto b = as.convert(11.0);
		auto c = a+b;

		std::map<double, unsigned int> map = as.getMap();
		for(std::map<double, unsigned int>::iterator iter = map.begin(); iter != map.end(); ++iter)
		{
			std::cout << iter->first << ": " << iter->second << std::endl;
		}
	}
}

/*
unsigned int Factorial( unsigned int number ) {
    return number <= 1 ? number : Factorial(number-1)*number;
}

TEST_CASE( "Factorials are computed", "[factorial]" ) {
    REQUIRE( Factorial(1) == 1 );
    REQUIRE( Factorial(2) == 2 );
    REQUIRE( Factorial(3) == 6 );
    REQUIRE( Factorial(10) == 3628800 );
}
*/

