#ifndef __TEST_C_H
#define __TEST_C_H

#include <check.h>

void tcase_core(Suite * s);
void tcase_interval(Suite * s);

#endif
