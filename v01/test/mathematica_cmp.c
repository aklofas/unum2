#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>

#include "wstp.h"

WSENV env = NULL;
WSLINK link = NULL;

static void deinit(void)
{
	if(link != NULL)
	{
		WSClose(link);
	}

	if(env != NULL)
	{
		WSDeinitialize(env);
	}
}

static void doerror(const char * message)
{
	if(link != NULL && WSError(link))
	{
		printf("Error detected: %s (WSTP: %s)\n", message, WSErrorMessage(link));
	}
	else
	{
		printf("Unknown error detected: %s\n", message);
	}

	exit(3);
}

static void waitreturn()
{
	int pkt;
	while((pkt = WSNextPacket(link), pkt) && pkt != RETURNPKT)
	{
		WSNewPacket(link);
		if (WSError(link))
		{
			doerror("Return packet error");
		}
	}
}

static void waitignore()
{
	waitreturn();
	WSNewPacket(link);
}


int main(int argc, char *argv[])
{
	atexit(deinit);

	env =  WSInitialize(NULL);
	if (env == NULL)
	{
		printf("Error initializing environment\n");
		exit(1);
	}

	int err;
	link = WSOpenArgcArgv(env, argc, argv, &err);
	if (link == NULL || err != WSEOK)
	{
		printf("Error opening Wolfram Link\n");
		exit(2);
	}

	WSPutFunction(link, "EvaluatePacket", 1);
	{
		WSPutFunction(link, "Import", 1);
		WSPutString(link, "../math/Unum.m");
	}
	WSEndPacket(link);
	WSFlush(link);
	waitignore();
	

	WSPutFunction(link, "EvaluatePacket", 1);
	{
		
		WSPutFunction(link, "TESTGenUnums", 3);
		WSPutInteger(link, 2);
		WSPutInteger(link, 7);
		WSPutString(link, "exacts");
	}
	WSEndPacket(link);
	WSFlush(link);
	waitreturn();


	while(WSReady(link))
	{
		switch (WSGetType(link))
		{
			case WSTKINT:
			{
				wsint64 i64;
				WSGetInteger64(link, &i64);
				printf("Integer: %"PRId64"\n", i64);
				break;
			}

			case WSTKREAL:
			{
				wsextended_double f128;
				WSGetReal128(link, &f128);
				printf("Real: %Lf\n", f128);
				break;
			}

			case WSTKSTR:
			{
				const char * str;
				WSGetString(link, &str);
				printf("String: %s\n", str);
				WSReleaseString(link, str);
				break;
			}

			case WSTKFUNC:
			{
				const char *fname;
				int nargs;
				WSGetFunction(link, &fname, &nargs);
				printf("Function: %s[%d]\n", fname, nargs);
				WSReleaseString(link, fname);
				break;
			}

			case WSTKSYM:
			{
				const char *sym;
				WSGetSymbol(link, &sym);
				printf("Symbol: %s\n", sym);
				WSReleaseSymbol(link, sym);
				break;
			}

			case WSTKERR:
				doerror("Expect return error");
			default:
				doerror("Unknown return error");
		}
	}


	WSPutFunction(link, "Exit", 0);
	printf("Done.\n");
	return 0;
}
