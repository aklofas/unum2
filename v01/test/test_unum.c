#include <stdio.h>
#include <math.h>

#include <unum.h>

#include "test_c.h"


//#define EPSILON		0.00005
#define EPSILON		0.04
//#define EPSILON		0.05
// TODO - IMPORTANT - get better bounds checking (4% is not that good)


START_TEST(test_infinity)
{
	unumd_t * ud = unum_new(1, 4);
	ck_assert_msg(ud != NULL, "Unum allocation failed.");

	unum_t d_inf = double2unum(ud, INFINITY), d_ninf = double2unum(ud, -INFINITY);
	unum_t f_inf = frac2unum(ud, frac_inf()), f_ninf = frac2unum(ud, frac_neg(frac_inf()));
	unum_t t_inf = 0x1 << (ud->nbits - 1);

	ck_assert_msg(d_inf == t_inf && d_ninf == t_inf, "Failed double to +-inf (+inf=0x%x, -inf=0x%x)", d_inf, d_ninf);
	ck_assert_msg(f_inf == t_inf && f_ninf == t_inf, "Failed fraction to +-inf (+inf=0x%x, -inf=0x%x)", f_inf, f_ninf);

	unum_free(ud);
}
END_TEST

START_TEST(test_double_convert)
{
	unumd_t * ud = unum_new(1, 7);
	ck_assert_msg(ud != NULL, "Unum allocation failed.");
	for (double x = -25.0; x <= 25.0; x += 0.01)
	{
		unum_t xu = double2unum(ud, x);
		//range_t xr = unum2range(ud, xu);

		sorn_t xs = unum2sorn(ud, xu);
		interval_t xi = sorn2interval(ud, xs);

		interval_t ti = interval_epsilon(x, fabs(x) * EPSILON + EPSILON);

		if (!interval_overlap(&xi, &ti))
		{
			char buf[35];
			interval_snprintf(&xi, buf, sizeof(buf));

			ck_abort_msg("Failed to convert %f. (not in %s) 0x%x -> (0x%x, 0x%x)", x, buf, xu, xs.low, xs.high);
		}
	}
	unum_free(ud);
}
END_TEST

START_TEST(test_negatives)
{
	unumd_t * ud = unum_new(1, 7);
	ck_assert_msg(ud != NULL, "Unum allocation failed.");

	for (double x = -15.0; x <= 15.0; x += 0.01)
	{
		unum_t xu = double2unum(ud, x);
		unum_t nxu = unum_neg(ud, xu);

		sorn_t nxs = unum2sorn(ud, nxu);
		interval_t nxi = sorn2interval(ud, nxs);

		interval_t nti = interval_epsilon(-x, fabs(x) * EPSILON + EPSILON);

		if (!interval_overlap(&nxi, &nti))
		{
			char buf[35];
			interval_snprintf(&nxi, buf, sizeof(buf));

			ck_abort_msg("Failed to negate %f. (not in %s) 0x%x -> (0x%x, 0x%x)", x, buf, nxu, nxs.low, nxs.high);
		}
	}

	unum_free(ud);
}
END_TEST

START_TEST(test_reciprocation)
{
	unumd_t * ud = unum_new(1, 7);
	ck_assert_msg(ud != NULL, "Unum allocation failed.");

	for (double x = -15.0; x <= 15.0; x += 0.01)
	{
		unum_t xu = double2unum(ud, x);
		unum_t oxu = unum_recip(ud, xu);

		sorn_t oxs = unum2sorn(ud, oxu);
		interval_t oxi = sorn2interval(ud, oxs);

		interval_t oti = interval_epsilon(1.0/x, fabs(1.0/x) * EPSILON + EPSILON);

		if (unum_iszero(ud, xu))
		{
			 if (!unum_ispminf(ud, oxu))
			 {
				interval_t xr = sorn2interval(ud, unum2sorn(ud, xu));
				char buf1[35];
				interval_snprintf(&xr, buf1, sizeof(buf1));

				char buf2[35];
				interval_snprintf(&oxi, buf2, sizeof(buf2));

				ck_abort_msg("Failed to reciprocate %.50f %s (0x%x). (+/-inf not in %s) 0x%x -> (0x%x, 0x%x)", x, buf1, xu, buf2, oxu, oxs.low, oxs.high);
			 }
		}
		else if (!interval_overlap(&oxi, &oti))
		{
			interval_t xr = sorn2interval(ud, unum2sorn(ud, xu));
			char buf1[35];
			interval_snprintf(&xr, buf1, sizeof(buf1));

			char buf2[35];
			interval_snprintf(&oxi, buf2, sizeof(buf2));

			ck_abort_msg("Failed to reciprocate %.50f %s (0x%x). (%f not in %s) 0x%x -> (0x%x, 0x%x)", x, buf1, xu, 1.0/x, buf2, oxu, oxs.low, oxs.high);
		}
	}

	unum_free(ud);
}
END_TEST

START_TEST(test_sweep_add)
{
	unumd_t * ud = unum_new(1, 7);
	ck_assert_msg(ud != NULL, "Unum allocation failed.");

	for (double a = -6.0; a < 6.0; a += 0.1)
	{
		for (double b = -6.0; b < 6.0; b += 0.1)
		{
			unum_t au = double2unum(ud, a);
			unum_t bu = double2unum(ud, b);

			sorn_t ab = unum_add(ud, au, bu);
			interval_t abi = sorn2interval(ud, ab);

			interval_t ti = interval_epsilon(a+b, fabs(a+b) * EPSILON + EPSILON);

			#if 0
			{
				interval_t ar = unum2interval(ud, au);
				interval_t br = unum2interval(ud, bu);

				char ar_b[30], br_b[30], abr_b[30];
				interval_snprintf(&ar, ar_b, sizeof(ar_b));
				interval_snprintf(&br, br_b, sizeof(br_b));
				interval_snprintf(&abi, abr_b, sizeof(abr_b));

				printf("%s %f + %f = %f \t\t\t %s + %s = %s\n", (!inrange(&abi, a+b, ACCURACY)? "ERR =>" : "      "), a, b, a+b, ar_b, br_b, abr_b);
			}
			#endif

			if (!interval_overlap(&abi, &ti))
			{
				interval_t ar = unum2interval(ud, au);
				interval_t br = unum2interval(ud, bu);

				char ar_b[30], br_b[30], abr_b[30];
				interval_snprintf(&ar, ar_b, sizeof(ar_b));
				interval_snprintf(&br, br_b, sizeof(br_b));
				interval_snprintf(&abi, abr_b, sizeof(abr_b));

				ck_abort_msg("Failed to add: %f + %f = %f   %s + %s = %s", a, b, a+b, ar_b, br_b, abr_b);
			}
		}
	}

	unum_free(ud);
}
END_TEST

START_TEST(test_scratch)
{
	unumd_t * ud = unum_new(1, 7);
	ck_assert_msg(ud != NULL, "Unum allocation failed.");

	sorn_t a = fracs2sorn(ud, frac_new(3,7), frac_new(-3,7));
	sorn_t b = frac2sorn(ud, frac_new(5,2));

	sorn_t ab = sorn_add(ud, a, b);
	interval_t abi = sorn2interval(ud, ab);

	char buf[50];
	interval_snprintf(&abi, buf, sizeof(buf));

	printf("Out >> %s\n", buf);

	unum_free(ud);
}
END_TEST


void tcase_core(Suite * s)
{
	TCase * tc_core = tcase_create("Core");
	tcase_add_test(tc_core, test_scratch);

	/*
	tcase_add_test(tc_core, test_infinity);
	tcase_add_test(tc_core, test_double_convert);
	tcase_add_test(tc_core, test_negatives);
	tcase_add_test(tc_core, test_reciprocation);
	tcase_add_test(tc_core, test_sweep_add);
	*/

	suite_add_tcase(s, tc_core);
}

