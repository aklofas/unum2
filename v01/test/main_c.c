#include <stdlib.h>

#include "test_c.h"


int main(void)
{
	SRunner *sr;

	Suite * s = suite_create("Unum2");
	tcase_core(s);
	tcase_interval(s);

	sr = srunner_create(s);
	srunner_run_all(sr, CK_NORMAL);
	int failed = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

/*
int main(int argc, char const *argv[])
{
	//double b1 = -INFINITY;
	//double a1 = 0;
	//printf(">> %f + %f = %f \n", a1, b1, a1+b1);

	#if 0
		// Fraction test
		{
			fraction_t a = frac_new(1, 2);
			fraction_t b = frac_new(2, 3);

			fraction_t apb = frac_add(a, b);
			fraction_t amb = frac_sub(a, b);
			printf("Fraction a+b = %ld/%ld\n", num(apb), denom(apb));
			printf("Fraction a-b = %ld/%ld\n", num(amb), denom(amb));
		}
	#endif


	unumd_t * ud = unum_new(1, 7);
	if (ud == NULL)
	{
		printf("Allocation failed!\n");
		exit(1);
	}

	#if 0
	printf("Print exacts fraction unums\n");
	{
		printf("Number unums=%zu\n", ud->ns);
		for (int i=0; i<ud->ns/2; i++)
		{
			char buf[25];
			frac_snprintf(&ud->exactsf[i], buf, sizeof(buf));
			printf("(%d) %s\n", i, buf);
		}
	}
	#endif

	#if 0
	printf("Print exacts double unums\n");
	{
		printf("Number unums=%zu\n", ud->ns);
		for (int i=0; i<ud->ns/2; i++)
		{
			printf("(%d) %f\n", i, ud->exactsd[i]);
		}
	}
	#endif


	#if 0
	printf("A/B unum test\n");
	{
		//double a = 4.9;
		//double b = -0.39;
		
		double a = 5.0;
		double b = -0.4;

		//double a = 1.0;
		//double b = 3.0;

		unum_t au = double2unum(ud, a);
		unum_t bu = double2unum(ud, b);

		printf("a->(%f) hex=%x, isexact=%d, value=%f\n", a, au, isexact(ud, au), unum2exactdouble(ud, au));
		printf("b->(%f) hex=%x, isexact=%d, value=%f\n", b, bu, isexact(ud, bu), unum2exactdouble(ud, bu));

		range_t ar = unum2range(ud, au);
		range_t br = unum2range(ud, bu);

		char ar_buf[25];
		char br_buf[25];
		range_snprintf(&ar, ar_buf, sizeof(ar_buf));
		range_snprintf(&br, br_buf, sizeof(br_buf));

		//printf("a->(%f) min=%f (cl?=%d), max=%f (cl?=%d)\n", a, ar.low, ar.lowi == closed, ar.high, ar.highi == closed);
		//printf("b->(%f) min=%f (cl?=%d), max=%f (cl?=%d)\n", b, br.low, br.lowi == closed, br.high, br.highi == closed);

		//sorn_t sab = unum2sorn(ud, au, bu);
		//sorn_t sba = unum2sorn(ud, bu, au);
		//range_t abr = sorn2range(ud, sab);
		//range_t bar = sorn2range(ud, sba);

		//printf("sorn a->b min=%f (cl?=%d), max=%f (cl?=%d)\n", abr.low, abr.lowi == closed, abr.high, abr.highi == closed);
		//printf("sorn b->a min=%f (cl?=%d), max=%f (cl?=%d)\n", bar.low, bar.lowi == closed, bar.high, bar.highi == closed);

		sorn_t a_add_b = unum_add(ud, au, bu);
		sorn_t a_sub_b = unum_sub(ud, au, bu);
		range_t a_add_b_r = sorn2range(ud, a_add_b);
		range_t a_sub_b_r = sorn2range(ud, a_sub_b);

		char buf[25];

		range_snprintf(&a_add_b_r, buf, sizeof(buf));
		printf("sorn a + b: %s + %s = %s\n", ar_buf, br_buf, buf);
		
		range_snprintf(&a_sub_b_r, buf, sizeof(buf));
		printf("sorn a - b: %s - %s = %s\n", ar_buf, br_buf, buf);
	}
	#endif


	#if 0
	printf("[2, 4]; x - x test\n");
	{
		sorn_t x = double2sorn(ud, 2, 4);
		for (int i=0; i < 10; i++)
		{
			range_t xr = sorn2range(ud, x);

			char buf[25];
			range_snprintf(&xr, buf, sizeof(buf));
			printf("sorn x: %s\n", buf);

			x = sorn_sub(ud, x, x);
		}
	}
	#endif

	unum_free(ud);

	return 0;
}
*/
